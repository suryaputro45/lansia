-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Agu 2023 pada 16.26
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lansia`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `app`
--

CREATE TABLE `app` (
  `idapp` int(11) NOT NULL,
  `namaapp` varchar(75) NOT NULL,
  `telpapp` varchar(13) NOT NULL,
  `logoapp` varchar(250) NOT NULL,
  `statusapp` int(2) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `app`
--

INSERT INTO `app` (`idapp`, `namaapp`, `telpapp`, `logoapp`, `statusapp`, `deskripsi`) VALUES
(1, 'lansia', '089776552763', '1690380275_b3608ba0beca33845956.png', 1, 'app ini adalah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `kdmenu` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `namamenu` varchar(150) NOT NULL,
  `icon` varchar(150) NOT NULL,
  `linkmenu` varchar(150) NOT NULL,
  `target` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`kdmenu`, `title`, `namamenu`, `icon`, `linkmenu`, `target`, `keterangan`, `status`, `role`) VALUES
(1, 'Master', 'Master', 'fas fa-inbox', '', 0, 'Judul Menu', 1, 1),
(2, 'Master', 'Master', 'fas fa-inbox', 'ap', 1, 'submenu', 1, 1),
(50, 'Setting Admin ', 'Setting Admin ', 'fas fa-cogs', '', 0, 'Judul Menu', 1, 1),
(51, 'Setting APP', 'Setting APP', 'fas fa-cogs', 'settingapp', 50, 'submenu', 1, 1),
(100, 'Setting Super Admin ', 'Setting Super Admin ', 'fas fa-cogs', '', 0, 'Judul Menu', 1, 0),
(101, 'Data Menu', 'Data Menu', 'fas fa-cogs', 'datamenu', 100, 'submenu', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `iduser` int(11) NOT NULL,
  `idpasien` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `nomorhp` varchar(13) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`iduser`, `idpasien`, `nama`, `alamat`, `nomorhp`, `status`) VALUES
(1, 1, 'bukan', '122', '12', 1),
(1, 2, 'bukanaku', 'jakarta ya', '12', 1),
(1, 3, '121', '122', '12', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusterapis`
--

CREATE TABLE `statusterapis` (
  `idstatus` int(11) NOT NULL,
  `namastatus` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `statusterapis`
--

INSERT INTO `statusterapis` (`idstatus`, `namastatus`, `status`) VALUES
(1, 'awal', 1),
(2, 'lanjut', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `terapi`
--

CREATE TABLE `terapi` (
  `idterapi` int(11) NOT NULL,
  `namaterapi` varchar(100) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `terapi`
--

INSERT INTO `terapi` (`idterapi`, `namaterapi`, `status`) VALUES
(1, 'remince', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksiterapis`
--

CREATE TABLE `transaksiterapis` (
  `idtranterapis` int(11) NOT NULL,
  `idapp` int(11) NOT NULL,
  `idpasien` int(11) NOT NULL,
  `idterapi` int(11) NOT NULL,
  `statusterapis` int(11) NOT NULL,
  `document1` varchar(150) NOT NULL,
  `document2` varchar(150) NOT NULL,
  `document3` varchar(150) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `transaksiterapis`
--

INSERT INTO `transaksiterapis` (`idtranterapis`, `idapp`, `idpasien`, `idterapi`, `statusterapis`, `document1`, `document2`, `document3`, `status`) VALUES
(1, 1, 1, 1, 1, '1', '1', '1', 1),
(2, 1, 1, 1, 1, '1691677412_2221407c10cf0b8709ff.pdf', '1691677412_8a72858c2422aa86d483', '', 1),
(3, 1, 1, 1, 1, '1691677485_1488b7997cce1782c517.pdf', '1691677485_c81a4d8938572ffd7444', '', 1),
(4, 1, 2, 1, 2, '1691677512_ab32f4f4de901bf4c69a.pdf', '1691677512_502771ef316ede80414c', '', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `namapanti` varchar(150) NOT NULL,
  `namaketuapanti` varchar(150) NOT NULL,
  `namaterapis` varchar(150) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `nomerhp` varchar(14) NOT NULL,
  `email` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `roleid` int(4) NOT NULL,
  `isactive` int(4) NOT NULL,
  `datecreate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `namapanti`, `namaketuapanti`, `namaterapis`, `alamat`, `nomerhp`, `email`, `image`, `name`, `password`, `roleid`, `isactive`, `datecreate`) VALUES
(1, '123', '321', '231', '1234', '111', '123@gmail.com', 'default.jpg', '', '123', 2, 1, '2023-07-15 04:53:54'),
(4, '123', '123', '123', '123', '123', '123@ymail.com', 'default.jpg', '', '123', 2, 1, '2023-07-15 05:03:25'),
(5, '123', '123', '123', '123', '123', '123@i.com', 'default.jpg', '', '123', 2, 1, '2023-07-15 05:04:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `userrole`
--

CREATE TABLE `userrole` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `userrole`
--

INSERT INTO `userrole` (`id`, `role`) VALUES
(1, 'administrator'),
(2, 'member');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `app`
--
ALTER TABLE `app`
  ADD PRIMARY KEY (`idapp`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`kdmenu`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`idpasien`);

--
-- Indeks untuk tabel `statusterapis`
--
ALTER TABLE `statusterapis`
  ADD PRIMARY KEY (`idstatus`);

--
-- Indeks untuk tabel `terapi`
--
ALTER TABLE `terapi`
  ADD PRIMARY KEY (`idterapi`);

--
-- Indeks untuk tabel `transaksiterapis`
--
ALTER TABLE `transaksiterapis`
  ADD PRIMARY KEY (`idtranterapis`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `userrole`
--
ALTER TABLE `userrole`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `app`
--
ALTER TABLE `app`
  MODIFY `idapp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `kdmenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT untuk tabel `pasien`
--
ALTER TABLE `pasien`
  MODIFY `idpasien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `statusterapis`
--
ALTER TABLE `statusterapis`
  MODIFY `idstatus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `terapi`
--
ALTER TABLE `terapi`
  MODIFY `idterapi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `transaksiterapis`
--
ALTER TABLE `transaksiterapis`
  MODIFY `idtranterapis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `userrole`
--
ALTER TABLE `userrole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

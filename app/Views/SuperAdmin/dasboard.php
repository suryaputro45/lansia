<?= $this->extend('tamplate/layoutsuperadmin') ?>

<?= $this->section('navbar') ?>
<?= view('SuperAdmin/navbarsuperadmin') ?>
<?= $this->endSection() ?>

<?= $this->section('isi') ?>
<div class="container mt-3">
    <div class="card">
        <div class="card-header">
            <i class="fas fa-inbox"></i> Master
        </div>
        <div class="card-body">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-3 text-center">
                <div class="col ">
                    <a href="ajaib" style="text-decoration: none;">
                        <div class="card h-100">
                            <div class="card-header h-100" style="color: blue;">users</div>
                            <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt" style="color: blue;"></i></span></div>
                        </div>
                    </a>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">management </div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span> </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">profil user satu dua </div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span></div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">management users</div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span> </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">management users</div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span> </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">management users apa itu maksunya</div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span> </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">management users</div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span> </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="card mt-3">
        <div class="card-header">
            <i class="fas fa-cogs"></i> Setting Admin
        </div>
        <div class="card-body">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-3 text-center">
                <div class="col ">
                    <a href="ajaib" style="text-decoration: none;">
                        <div class="card h-100">
                            <div class="card-header h-100" style="color: blue;">users</div>
                            <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt" style="color: blue;"></i></span></div>
                        </div>
                    </a>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">management </div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span> </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">profil user satu dua </div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span></div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">management users</div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span> </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">management users</div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span> </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">management users apa itu maksunya</div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span> </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <div class="card-header h-100">management users</div>
                        <div class="p-3"><span style="font-size: 50px;"><i class="fas fa-user-alt"></i></span> </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>
</div>
<?= $this->endSection() ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <script src="<?= base_url('/') ?>/assets/js/color-modes.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.112.5">
    <title>Headers · Bootstrap v5.3</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.3/examples/headers/">
    <link href="<?= base_url('/') ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="<?= base_url('/') ?>/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="<?= base_url('/') ?>/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="<?= base_url('/') ?>/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="<?= base_url('/') ?>/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="<?= base_url('/') ?>/assets/img/favicons/safari-pinned-tab.svg" color="#712cf9">
    <link rel="icon" href="<?= base_url('/') ?>/assets/img/favicons/favicon.ico">
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/bootstrap/fontawesome-free/css/all.min.css" />

    <meta name="theme-color" content="#712cf9">
    <style>
        .nav-item .active {
            background-color: #ccc;
            border-radius: 10px;
        }
    </style>
    <style>
        .nav-item .active {
            background-color: #ccc;
            border-radius: 10px;
        }

        body::before {
            content: "";
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('<?= base_url('/assets/images/TanpaJudul.png') ?>');
            opacity: 0.2;
            z-index: -1;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
        }
    </style>
</head>

<body>

    <nav class="navbar navbar-expand-sm navbar-primary bg-primary">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="<?= base_url('/assets/images/' . $infoApp[0]['logoapp']) ?>" alt="Avatar Logo" style="width:60px;" class="rounded-pill">
            </a>
            <!-- <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasWithBothOptions">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="offcanvas offcanvas-start text-bg-primary" data-bs-scroll="true" tabindex="-1" id="offcanvasWithBothOptions" aria-labelledby="offcanvasWithBothOptionsLabel">
                <div class="navbar-collapse" id="offcanvasWithBothOptions">
                    <div class="offcanvas-header">
                        <h5 class="offcanvas-title" id="offcanvasScrollingLabel">Offcanvas with body scrolling</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <ul class="navbar-nav me-auto justify-content-center">
                        <ul class="navbar-nav me-auto justify-content-center">
                            <li class="nav-item">
                                <a class="nav-link active" href="javascript:void(0)">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Features</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">Pricing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">FAQs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">About</a>
                            </li>
                        </ul>
                    </ul>
                    <form class="d-flex mb-2 mt-2">

                        <div class="input-group me-2">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                            <input type="text" class="form-control" placeholder="Search">
                        </div>

                    </form>
                    <a type="button" href="<?= base_url('/') ?>login" class="btn btn-outline-light me-2 ">Login</a>
                    <button type="button" class="btn btn-warning">Sign-up</button>
                </div>
            </div> -->
        </div>
    </nav>
    <div class="container mt-3">
        <div class="card">
            <div class="card-body">
                <h3><?= strtoupper($infoApp[0]['namaapp']); ?></h3>
                <p><?= ucfirst($infoApp[0]['deskripsi']); ?></p>
                <div class="col">
                    <a type="button" href="<?= base_url('/') ?>login" class="btn btn-primary me-2 ">Login</a>
                    <a type="button" href="<?= base_url('/') ?>daftar" class="btn btn-warning me-2 ">Login</a>
                    <!-- <button type="button" class="btn btn-warning">Sign-up</button> -->
                </div>
            </div>

        </div>

    </div>
    <script src="<?= base_url('/') ?>assets/bootstrap/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>

</body>

</html>
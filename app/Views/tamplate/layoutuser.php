<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="theme-color" content="#000000" />
    <title>Mobilekit Mobile UI Kit</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit" />
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="<?= base_url('/') ?>assets/user/img/favicon.png" sizes="32x32" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('/') ?>assets/user/img/icon/192x192.png" />
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/user/css/users/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/user/css/users/owl-carousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/user/css/users/owl-carousel/owl.theme.default.css" />
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:400,500,700&display=swap" /> -->
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/user/fontawesome-free/css/all.min.css" />
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/user/css/users/style.css" />

    <script src="<?= base_url('/') ?>assets/bootstrap/js/alert/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/bootstrap/js/alert/sweetalert2.min.css">


    <!-- Jquery -->
    <script src="<?= base_url('/') ?>assets/bootstrap/js/jquery_jquery-3.7.0.js"></script>
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/user/datatables/datatables_1.13.5_css_jquery.dataTables.min.css" />
    <script src="<?= base_url('/') ?>assets/user/datatables/jquery_dataTables.min.js"></script>
    <!-- alert -->
    <script src="<?= base_url('/') ?>assets/bootstrap/js/alert/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/bootstrap/js/alert/sweetalert2.min.css">
</head>
<style>
    .nav-item .active {
        background-color: #ccc;
        border-radius: 10px;
    }

    body::before {
        content: "";
        display: block;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-image: url('<?= base_url('/assets/images/TanpaJudul.png') ?>');
        opacity: 0.2;
        z-index: -1;
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>

<body style="background-color: #e9ecef">
    <?php if (session()->get('logged_in')) : ?>
        <?= $this->renderSection('isi'); ?>
        <div class="appBottomMenu">
            <a href="<?= base_url('/') ?>user" class="item">
                <div class="col">
                    <i class="fas fa-home fa-3x"></i>
                    <strong>Beranda</strong>
                </div>
            </a>

            <a href="<?= base_url('/') ?>terapi" class="item">
                <div class="col">
                    <i class="fas fa-file-alt fa-3x"></i>
                    <strong>Terapi</strong>
                </div>
            </a>
            <a href="<?= base_url('/') ?>dataterapi" class="item">
                <div class="col">
                    <i class="fas fa-user-tie fa-3x"></i>
                    <strong>Data</strong>
                </div>
            </a>
            <a href="<?= base_url('/') ?>profil" class="item">
                <div class="col">
                    <i class="fas fa-user-tie fa-3x"></i>
                    <strong>Saya</strong>
                </div>
            </a>
        </div>


    <?php else : ?>
        <p>You are not logged in. Please <a href="<?= site_url('login'); ?>">login</a>.</p>
    <?php endif; ?>


    <!-- App Bottom Menu -->

    <!-- * App Bottom Menu -->
    <!-- * App Bottom Menu -->

    <!-- ///////////// Js Files ////////////////////  -->

    <!-- Bootstrap-->
    <script src="<?= base_url('/') ?>assets/user/js/boostrap/popper.min.js"></script>
    <script src="<?= base_url('/') ?>assets/user/js/boostrap/bootstrap.bundle.min.js"></script>
    <!-- Chart JS -->
    <script src="<?= base_url('/') ?>assets/user/chart/dist/chart.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/chart.js"></script> -->
    <!-- Owl Carousel -->
    <script src="<?= base_url('/') ?>assets/user/js/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="<?= base_url('/') ?>assets/user/js/owl-carousel/circle-progress.min.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
    <!-- Base Js File -->
    <script src="<?= base_url('/') ?>assets/user/js/owl-carousel/base.js"></script>

    <script>
        const ctx = document.getElementById('myChart');

        new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    borderWidth: 1
                }]
            }
        });
    </script>
</body>

</html>
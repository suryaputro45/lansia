<!doctype html>
<html lang="en" data-bs-theme="auto">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.112.5">
    <title><?= $judul ?></title>


    <script src="<?= base_url('/') ?>assets/bootstrap/js/alert/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/bootstrap/js/alert/sweetalert2.min.css">

    <link href="<?= base_url('/') ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="<?= base_url('/') ?>assets/bootstrap/css/style.css" rel="stylesheet"> -->
    <link href="<?= base_url('/') ?>assets/bootstrap/css/style2.css" rel="stylesheet">
    <script src="<?= base_url('/') ?>assets/bootstrap/js/jquery-3.6.0.min.js"></script>




    <meta name="theme-color" content="#712cf9">
</head>


<body>
    <?= $this->renderSection('content') ?>

    <script src="<?= base_url('/') ?>assets/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

<!--  -->

</html>
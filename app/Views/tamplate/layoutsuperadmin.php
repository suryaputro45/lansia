<!DOCTYPE html>
<html lang="en">

<head>
    <script src="<?= base_url('/') ?>/assets/js/color-modes.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.112.5">
    <title>Headers · Bootstrap v5.3</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.3/examples/headers/">
    <link href="<?= base_url('/') ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="<?= base_url('/') ?>/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="<?= base_url('/') ?>/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="<?= base_url('/') ?>/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="<?= base_url('/') ?>/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="<?= base_url('/') ?>/assets/img/favicons/safari-pinned-tab.svg" color="#712cf9">
    <link rel="icon" href="<?= base_url('/') ?>/assets/img/favicons/favicon.ico">
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/bootstrap/fontawesome-free/css/all.min.css" />

    <meta name="theme-color" content="#712cf9">
    <style>
        .nav-item .active {
            background-color: #ccc;
            border-radius: 10px;
        }

        body::before {
            content: "";
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('<?= base_url('/assets/images/TanpaJudul.png') ?>');
            opacity: 0.2;
            z-index: -1;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
        }
    </style>
</head>

<body>
    <?= $this->renderSection('navbar') ?>
    <?= $this->renderSection('isi') ?>


    <script src="<?= base_url('/') ?>assets/bootstrap/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>

</body>

</html>
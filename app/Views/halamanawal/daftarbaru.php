<?= $this->extend('tamplate/layoutlogin') ?>

<?= $this->section('content') ?>
<style>
    .parent-container {
        display: flex;
        justify-content: center;
    }

    .error-input {
        border-color: red;
    }
</style>

<div class="wrapper">
    <?php if (session()->get('errors')) : ?>
        <script src="path_to_sweetalert2_js"></script>
        <script>
            // Tampilkan pesan error menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Terjadi kesalahan pada inputan:',
                html: "<?php echo implode('<br>', session()->get('errors')) ?>"
            });
        </script>
    <?php endif; ?>
    <?php if (session()->getFlashdata('error')) : ?>
        <!-- <script src="path_to_sweetalert2_js"></script> -->
        <script>
            // Tampilkan pesan sukses menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Kode uniq tidak Valid',
                text: "<?php echo session()->getFlashdata('error'); ?>"
            });
        </script>
    <?php endif; ?>
    <div class="parent-container">

        <div class="col-lg-7">
            <div class="card  o-hidden border-0 shadow-lg-7 my-5">
                <div class="card-body p-0">

                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Daftar Akun</h1>
                                </div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-bs-toggle="tab" href="#home">Panti</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-bs-toggle="tab" href="#menu1">Pengguna</a>
                                    </li>

                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div id="home" class="container tab-pane active"><br>
                                        <form class="user" method="POST" action="<?= base_url('/proses') ?>">
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <input type="text" value="<?= old('namapanti') ?>" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['namapanti'])) ? 'is-invalid' : ''; ?>" name="namapanti" id="namapanti" placeholder="Nama Panti">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<?= old('namaketuapanti') ?>" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['namaketuapanti'])) ? 'is-invalid' : ''; ?>" id="namaketuapanti" name="namaketuapanti" placeholder="Nama Ketua Panti">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <input type="text" value="<?= old('namaterapis') ?>" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['namaterapis'])) ? 'is-invalid' : ''; ?>" id="namaterapis" name="namaterapis" placeholder="Nama Terapis">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" value="<?= old('nomerhp') ?>" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['nomerhp'])) ? 'is-invalid' : ''; ?>" id="nomerhp" name="nomerhp" placeholder="Nomer Hp(aktif)">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" value="<?= old('email') ?>" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['email'])) ? 'is-invalid' : ''; ?>" id="email" name="email" placeholder="Email Address">

                                            </div>
                                            <div class="form-group">
                                                <textarea class=" form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['alamat'])) ? 'is-invalid' : ''; ?>" name="alamat" id="alamat" cols="30" rows="2" placeholder="Alamat Panti"><?= old('alamat') ?></textarea>
                                                <!-- <input type="" class="form-control form-control-user" id="exampleInputEmail" placeholder="Email Address"> -->
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <input type="password" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['alamat'])) ? 'is-invalid' : ''; ?> " name="password" id="password" placeholder="Password">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="password" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['alamat'])) ? 'is-invalid' : ''; ?>" name="repeat_password" id="repeat_password" placeholder="Repeat Password">
                                                </div>
                                            </div>
                                            <button type="submit" id="daftarbaru" class="btn btn-primary btn-user btn-block">
                                                Daftar
                                            </button>

                                        </form>
                                    </div>
                                    <div id="menu1" class="container tab-pane fade"><br>
                                        <form class="user" method="POST" action="<?= base_url('/daftarpengguna') ?>">

                                            <div class="form-group row">
                                                <div class="col">
                                                    <input type="text" value="<?= old('namalansia') ?>" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['namalansia'])) ? 'is-invalid' : ''; ?>" id="namalansia" name="namalansia" placeholder="Nama Lengkap Lansia">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" value="<?= old('email') ?>" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['email'])) ? 'is-invalid' : ''; ?>" id="email" name="email" placeholder="Email Yang Masih Aktif">

                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <input type="password" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['password'])) ? 'is-invalid' : ''; ?> " name="password" id="password" placeholder="Password">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="password" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['repeat_password'])) ? 'is-invalid' : ''; ?>" name="repeat_password" id="repeat_password" placeholder="Repeat Password">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user <?php echo (session()->get('errors') && isset(session()->get('errors')['kodepanti'])) ? 'is-invalid' : ''; ?>" name="kodepanti" id="kodepanti" placeholder="Kode Akses Khusus">

                                            </div>
                                            <button type="submit" id="daftarbaru" class="btn btn-primary btn-user btn-block">
                                                Daftar
                                            </button>

                                        </form>
                                    </div>
                                </div>

                                <hr>
                                <div class="text-center">
                                    <a class="small" href="forgot-password.html">Lupa kata sandi?</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="<?= base_url('/login') ?>">Sudah memiliki akun? Login!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script>
        $('#nomerhp').keyup(function(e) {
            if (/\D/g.test(this.value)) {
                // Filter non-digits from input value.
                this.value = this.value.replace(/\D/g, '');
            }
        });
        $(document).ready(function() {
            // Mengambil referensi ke input password pertama dan kedua
            var passwordInput = $("#password");
            var repeatPasswordInput = $("#repeat_password");

            // Menambahkan event listener untuk memvalidasi input saat mengubah nilainya
            repeatPasswordInput.on("input", validatePasswords);

            function validatePasswords() {
                var passwordValue = passwordInput.val();
                var repeatPasswordValue = repeatPasswordInput.val();

                if (passwordValue !== repeatPasswordValue) {
                    // Jika password tidak sesuai, tambahkan class "is-invalid" untuk menampilkan visualisasi bahwa password tidak sesuai
                    repeatPasswordInput.addClass("is-invalid");
                    // button disebel
                    $('#daftarbaru').prop('disabled', true);
                } else {
                    // Jika password sesuai, hapus class "is-invalid" jika sebelumnya telah ditambahkan
                    repeatPasswordInput.removeClass("is-invalid");
                    $('#daftarbaru').prop('disabled', false);
                }
            }
        });
    </script>








    <?= $this->endSection() ?>
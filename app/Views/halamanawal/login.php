<?= $this->extend('tamplate/layoutlogin') ?>

<?= $this->section('content') ?>
<style>
    .parent-container {
        display: flex;
        justify-content: center;
    }
</style>
<?php if (session()->get('errors')) : ?>
    <!-- <script src="path_to_sweetalert2_js"></script> -->
    <script>
        // Tampilkan pesan error menggunakan SweetAlert2
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Terjadi kesalahan pada inputan:',
            html: "<?php echo '<br>', session()->getFlashdata('errors'); ?>"
        });
    </script>
<?php endif; ?>
<?php if (session()->getFlashdata('success')) : ?>
    <script src="path_to_sweetalert2_js"></script>
    <script>
        // Tampilkan pesan sukses menggunakan SweetAlert2
        Swal.fire({
            icon: 'success',
            title: 'Pendaftaran Berhasil!',
            text: "<?php echo session()->getFlashdata('success'); ?>"
        });
    </script>
<?php endif; ?>
<div class="wrapper">
    <div class="parent-container">
        <div class="col-lg-7">
            <div class="card  o-hidden border-0 shadow-lg-7 my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Login</h1>
                                </div>
                                <form class="user" method="POST" action="<?= base_url('/ceklogin') ?>">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Password">
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Login
                                    </button>

                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="forgot-password.html">Lupa kata sandi?</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="<?= base_url('/daftar') ?>">Buat Akun Baru</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?= $this->endSection() ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" />

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .nav-item .active {
            background-color: #ccc;
            border-radius: 10px;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-primary bg-primary">
        <div class="container-fluid">
            <a class="navbar-brand" href="javascript:void(0)">Logo Kaaamu</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasWithBothOptions">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="offcanvas offcanvas-start text-bg-primary" data-bs-scroll="true" tabindex="-1" id="offcanvasWithBothOptions" aria-labelledby="offcanvasWithBothOptionsLabel">
                <div class="navbar-collapse" id="offcanvasWithBothOptions">
                    <div class="offcanvas-header">
                        <h5 class="offcanvas-title" id="offcanvasScrollingLabel">Offcanvas with body scrolling</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <ul class="navbar-nav me-auto justify-content-center">
                        <ul class="navbar-nav me-auto justify-content-center">
                            <li class="nav-item">
                                <a class="nav-link active" href="javascript:void(0)">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">Features</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">Pricing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">FAQs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">About</a>
                            </li>
                        </ul>
                    </ul>
                    <form class="d-flex mb-2 mt-2">

                        <div class="input-group me-2">
                            <span class="input-group-text"><i class="fa-solid fa-magnifying-glass"></i></span>
                            <input type="text" class="form-control" placeholder="Search">
                        </div>

                    </form>
                    <button type="button" class="btn btn-outline-light me-2 ">Login</button>
                    <button type="button" class="btn btn-warning">Sign-up</button>
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fluid mt-3">
        <h3>Navbar Forms</h3>
        <p>You can also include forms inside the navigation bar.</p>
    </div>

</body>

</html>
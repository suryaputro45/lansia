<?= $this->extend('tamplate/layoutadmin') ?>

<?= $this->section('navbar') ?>
<?= view('SuperAdmin/navbarsuperadmin') ?>
<?= $this->endSection() ?>

<?= $this->section('isi') ?>
<div class="container mt-3">
    <?php //print_r($judul);
    foreach ($judul as $val) :
    ?>
        <div class="card mt-3">
            <div class="card-header">
                <i class="<?= $val['icon'] ?>"></i> <?= $val['title'] ?>
            </div>
            <?php ?>
            <div class="card-body">
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-3 text-center">
                    <?php foreach ($menu as $row) :
                        if ($val['kdmenu'] == $row['target']) : ?>
                            <div class="col">
                                <a href="<?= $row['linkmenu'] ?>" style="text-decoration: none;">
                                    <div class="card h-100">
                                        <div class="card-header h-100" style="color: blue;"><?= $row['title'] ?></div>
                                        <div class="p-3"><span style="font-size: 50px;"><i class="<?= $row['icon'] ?>" style="color: blue;"></i></span></div>
                                    </div>
                                </a>
                            </div>
                    <?php endif;
                    endforeach ?>
                </div>
            </div>

        </div>
    <?php endforeach ?>
</div>


<?= $this->endSection() ?>
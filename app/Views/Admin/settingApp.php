<?= $this->extend('tamplate/layoutadmin') ?>

<?= $this->section('navbar') ?>
<?= view('SuperAdmin/navbarsuperadmin') ?>
<?= $this->endSection() ?>
<?= $this->section('isi') ?>
<div class="container mt-3">
    <script src="<?= base_url('/') ?>assets/bootstrap/js/alert/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="<?= base_url('/') ?>assets/bootstrap/js/alert/sweetalert2.min.css">

    <?php if (session()->get('errors')) : ?>
        <script>
            // Tampilkan pesan error menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Terjadi kesalahan pada inputan:',
                html: "<?php echo implode('<br>', session()->get('errors')) ?>"
            });
        </script>
    <?php endif; ?>
    <?php if (session()->getFlashdata('success')) : ?>
        <!-- <script src="path_to_sweetalert2_js"></script> -->
        <script>
            // Tampilkan pesan sukses menggunakan SweetAlert2
            Swal.fire({
                icon: 'success',
                title: 'Perubahan Berhasil!',
                text: "<?php echo session()->getFlashdata('success'); ?>"
            });
        </script>
    <?php endif; ?>
    <div class="card mt-3">
        <div class="card-header">
            <h4>Setting APP</h4>
        </div>
        <?php //var_dump($hasil[0]['namaapp']); 
        ?>
        <div class="card-body">
            <form method="post" action="<?= base_url('/') . 'updateApp' ?>" enctype="multipart/form-data">

                <input type="hidden" class="form-control" id="id" name="id" value="<?= $hasil[0]['idapp'] ?>">
                <input type="hidden" class="form-control" id="logo" name="logolama" value="<?= $hasil[0]['logoapp'] ?>">

                <div class="mb-3 row">
                    <label for="namaapp" class="col-sm-3 col-form-label">Nama Aplikasi</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="namaapp" name="namaapp" value="<?= $hasil[0]['namaapp'] ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="telpapp" class="col-sm-3 col-form-label">Nomer HP</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="telpapp" name="telpapp" value="<?= $hasil[0]['telpapp'] ?>">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="logoapp" class="col-sm-3 col-form-label">Logo Aplikasi</label>
                    <div class="col-sm-2">
                        <img src="/assets/images/<?= $hasil[0]['logoapp'] ?>" class="img-thumbnail img-preview" alt="logo.php">
                    </div>
                    <div class="col-sm-7">
                        <input class="form-control" type="file" id="logoapp" name="logoapp" value="<?= $hasil[0]['logoapp'] ?>" onchange="previewImg()">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="deskripsi" class="col-sm-3 col-form-label">Deskripsi</label>

                    <div class="col-sm-9">
                        <textarea class="form-control" id="deskripsi" name="deskripsi"><?= $hasil[0]['deskripsi'] ?></textarea>
                    </div>
                </div>
                <div class="d-flex justify-content-between">
                    <a href="/admin" type="button" class="btn btn-secondary btn-sm">Kembali</a>
                    <button type="submit" class="btn btn-success btn-sm">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function previewImg() {
        const urlGambar = document.querySelector('#logoapp');
        const imgPreview = document.querySelector('.img-preview');

        const fileGambar = urlGambar.files[0];

        const reader = new FileReader();

        reader.onload = function(e) {
            // Mengambil alamat gambar (URL data) dari file yang dipilih
            const urlGambarData = e.target.result;

            // Menampilkan pratinjau gambar di elemen img dengan class 'img-preview'
            imgPreview.src = urlGambarData;

            // Menggunakan urlGambarData sesuai kebutuhan Anda (misalnya: menyimpan di database atau mengirimkan ke server)
            // console.log('Alamat gambar: ', urlGambarData);
        }

        // Membaca file gambar sebagai URL data
        reader.readAsDataURL(fileGambar);
    }
</script>
<?= $this->endSection() ?>
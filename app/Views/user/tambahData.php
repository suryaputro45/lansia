<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>
<!-- loader -->
<style>
    b {
        font-weight: bold;
    }
</style>
<div id="loader">
    <div class="spinner-border text-primary" role="status"></div>
</div>
<!-- * loader -->

<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas  fa-2x">Tambah Data</i>
        </a>
    </div>
</div>
<!-- * App Header -->

<!-- App Capsule -->
<div id="appCapsule">
    <?php if (session()->get('errors')) : ?>
        <script>
            // Tampilkan pesan error menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Terjadi kesalahan pada inputan:',
                html: "<?php echo implode('<br>', session()->get('errors')) ?>"
            });
        </script>
    <?php endif; ?>
    <?php if (session()->getFlashdata('success')) : ?>
        <!-- <script src="path_to_sweetalert2_js"></script> -->
        <script>
            // Tampilkan pesan sukses menggunakan SweetAlert2
            Swal.fire({
                icon: 'success',
                title: 'Berhasil!',
                text: "<?php echo session()->getFlashdata('success'); ?>"
            });
        </script>
    <?php endif; ?>

    <div class="container">
        <div class="section full mt-4 mb-5">
            <div class="section-title">Title</div>
            <div class="wide-block pt-2 pb-2">
                <div>
                    <a href="newdata" class="btn btn-primary"><i class="fas fa-plus-circle" style="font-size: 24px; margin-right: 8px;"></i> Tambah Pasien</a>
                </div>
                <div class="table-responsive mt-3 ">

                    <table class="table-responsive" id="myTable">
                        <thead>
                            <td>NO</td>
                            <td>Nama Pasien</td>
                            <td>Alamat</td>
                            <td>Telpon</td>
                            <td>Aksi</td>
                        </thead>

                        <tbody>
                            <?php $no = 1;
                            // dd($data);
                            foreach ($data as $val) : ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><a href="<?= base_url('/') ?>detaildata/<?= $val['kode'] ?>"><?= $val['nama'] ?></a></td>
                                    <td><?= $val['alamat'] ?></td>
                                    <td><?= $val['notelp']  ?></td>
                                    <td><button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#edit<?= $val['idprofil'] ?>"> Edit</button>
                                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#hapus<?= $val['idprofil'] ?>"> Hapus</button>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
    <!-- <div class="card mt-2">
        ayo
    </div> -->

</div>
<!-- * App Capsule -->

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
<?php foreach ($data as $val) : ?>
    <!-- Modal -->
    <div class="modal fade" id="edit<?= $val['idprofil'] ?>" tabindex="-1" role="dialog" aria-labelledby="edit<?= $val['idprofil'] ?>Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="edit<?= $val['idprofil'] ?>Label">Edit Data Pasien</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('/updatedata') ?>" method="POST">
                        <input type="hidden" id="idprofil" value="<?= $val['idprofil'] ?>" class="form-control" name="idprofil">
                        <div class="row center">
                            <div class="col">
                                <input type="text" id="namapasien" value="<?= $val['nama'] ?>" class="form-control" name="namapasien" placeholder="Nama Lengkap Lansia">
                            </div>
                        </div>
                        <div class="row center mt-2">

                            <div class="col">
                                <input class="form-control" type="file" id="fotolansia" name="fotolansia" value="<?= $val['foto'] ?>">
                            </div>
                            <!-- </div> -->
                        </div>
                        <div class="row center mt-2">
                            <?php // dd($val) 
                            ?>
                            <div class="col">
                                <input type="text" id="tempatlahir" class="form-control" name="tempatlahir" value="<?= $val['tempatlahir'] ?>" placeholder="Tempat Lahir">
                            </div>
                            <div class="col">
                                <input type="date" id="tanggallahir" class="form-control" name="tanggallahir" value="<?= $val['tanggallahir'] ?>" placeholder="Tanggal Lahir">
                            </div>
                        </div>
                        <div class="row center mt-2">

                            <div class="col">
                                <input type="text" id="alamatpasien" class="form-control" name="alamatpasien" value="<?= $val['alamat'] ?>" placeholder="Alamat Rumah Asal">
                            </div>
                            <div class="col">
                                <input type="file" id="fotorumah" class="form-control" name="fotorumah" value="<?= $val['fotorumah']  ?>">

                            </div>

                        </div>

                        <div class="row center mt-2">

                            <div class="col">
                                <input type="text" id="namapasangan" class="form-control" name="namapasangan" value="<?= $val['namapasangan'] ?>" placeholder="Nama Suami/Istri">
                            </div>
                            <div class="col">
                                <input type="file" id="fotopasangan" class="form-control" name="fotopasangan" value="<?= $val['fotopasangan']  ?>" placeholder="ja">

                            </div>
                        </div>
                        <div class="row center mt-2">
                            <!-- <div class="col-md-3 ">
                                    <label for="alamatpasien" class="col-form-label">Alamat Pasien</label>
                                </div> -->
                            <div class="col">
                                <input type="text" id="namaanak" class="form-control" name="namaanak" value="<?= $val['anak'] ?>" placeholder="Nama Anak">
                            </div>
                            <div class="col">
                                <input type="file" id="fotoanak" class="form-control" name="fotoanak" value="<?= $val['fotoanak']  ?>">
                                <!-- <label for="fotoanak" style="display: block; ">
                                        <i class="fas fa-camera" style="font-size: 32px; margin-right: 8px;"></i>
                                        Foto Anak
                                    </label> -->
                            </div>
                        </div>
                        <div class="row center mt-2">
                            <!-- <div class="col-md-3 ">
                                    <label for="alamatpasien" class="col-form-label">Alamat Pasien</label>
                                </div> -->
                            <div class="col">
                                <input type="text" id="namasaudara" class="form-control" name="namasaudara" value="<?= $val['saudara'] ?>" placeholder="Nama Sanak Saudara">
                            </div>
                            <div class="col">
                                <input type="file" id="fotosaudara" class="form-control" name="fotosaudara" value="<?= $val['fotosaudara']  ?>">
                                <!-- <label for="fotosanaksaudara" style="display: block; ">
                                        <i class="fas fa-camera" style="font-size: 32px; margin-right: 8px;"></i>
                                        Foto Sanak Saudara
                                    </label> -->
                            </div>
                        </div>
                        <div class="row center mt-2">
                            <!-- <div class="col-md-3 ">
                                    <label for="alamatpasien" class="col-form-label">Alamat Pasien</label>
                                </div> -->
                            <div class="col">
                                <input type="text" id="namaingattempat" class="form-control" name="namaingattempat" value="<?= $val['kesantempat'] ?>" placeholder="Nama Tempat Paling Diingat">
                            </div>
                            <div class="col">
                                <input type="file" id="fotoingatempat" class="form-control" name="fotoingatempat" value="<?= $val['fotokesantempat'] ?>">
                                <!-- <label for="fotoingatempat" style="display: block; ">
                                        <i class="fas fa-camera" style="font-size: 32px; margin-right: 8px;"></i>
                                        Foto Tempat
                                    </label> -->
                            </div>
                        </div>
                        <div class="row center mt-2">
                            <div class="col-md-3 ">
                                <label for="telppasien" class="col-form-label">Nomor Hp</label>
                            </div>
                            <div class="col">
                                <input type="text" id="telppasien" class="form-control" name="telppasien" value="<?= $val['notelp'] ?>">
                            </div>
                        </div>
                        <div class=" mt-2"></div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-between">
                                <a href="/tambahdata" type="button" class="btn btn-secondary btn-sm">Kembali</a>
                                <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                            </div>
                        </div>
                        <div class=" mt-2"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>

<?php foreach ($data as $val) : ?>
    <!-- Modal -->
    <div class="modal fade" id="hapus<?= $val['idprofil'] ?>" tabindex="-1" role="dialog" aria-labelledby="edit<?= $val['idprofil'] ?>Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="edit<?= $val['idprofil'] ?>Label">Edit Data Pasien</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('/hapusData') ?>" method="POST">
                        <input type="hidden" id="idprofil" value="<?= $val['idprofil'] ?>" class="form-control" name="idprofil">
                        Apakah Anda akan menghapus data pasien "<b><?= $val['nama']; ?></b>"?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-primary">Ya</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach ?>
<?= $this->endSection() ?>
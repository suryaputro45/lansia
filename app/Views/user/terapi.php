<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>
<!-- loader -->
<div id="loader">
    <div class="spinner-border text-primary" role="status"></div>
</div>
<!-- * loader -->
<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas  fa-2x">Terapi</i>
        </a>
    </div>
</div>
<div class="pageTitle">Blank Page</div>
<div class="right"></div>
<!-- * App Header -->
<!-- App Capsule -->
<div id="appCapsule">

    <?php if (session()->get('errors')) : ?>
        <script src="path_to_sweetalert2_js"></script>
        <script>
            // Tampilkan pesan error menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Terjadi kesalahan pada inputan:',
                html: "<?php echo implode('<br>', session()->get('errors')) ?>"
            });
        </script>
    <?php endif; ?>
    <?php if (session()->getFlashdata('error')) : ?>
        <!-- <script src="path_to_sweetalert2_js"></script> -->
        <script>
            // Tampilkan pesan sukses menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Kode uniq tidak Valid',
                text: "<?php echo session()->getFlashdata('error'); ?>"
            });
        </script>
    <?php endif; ?>


    <div class="container">

        <div class="section full mt-2">
            <!-- <div class="section-title">Title</div> -->
            <div class="card mt-3 mb-5">

                <div class="wide-block pt-2 pb-2">


                    <div class="d-flex justify-content-between">
                        <!-- <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:60px;" class="rounded-pill"> -->
                        <button class="btn btn-sm btn-success"> <i class="fas fa-file-alt"><span> Menu Terapi</span></i></button>
                        <!-- <a href="/newterapi" class="btn btn-primary">Tambah Terapi</a> -->
                        <a href="newdata" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> Tambah Pasien</a>

                    </div>

                    <!-- <div class="card mt-2 mb-5"> -->
                    <div>
                    </div>

                    <div class="card-body">

                        <!-- DATA YANG AKAN DI TAMPILKAN DARI DATABASE -->
                        <div class="card">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Telp</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- looping isi dari database -->
                                    <?php $no = 1;
                                    foreach ($pengguna as $val) : ?>
                                        <tr>
                                            <th scope="row"><?= $no++ ?></th>
                                            <td><a href="<?= base_url('/detaildata') . '/' . $val['idprofil'] ?>"><?= $val['nama'] ?></a></td>
                                            <td><?= $val['notelp'] ?></td>
                                            <td><i class="fas fa-edit"></i></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>

                        </div>


                    </div>
                    <!-- </div> -->
                </div>



            </div>
            <!-- <div class="card mt-2">
        ayo
    </div> -->
        </div>
    </div>
    <!-- * App Capsule -->
    <?= $this->endSection() ?>
<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>

<?php if (session()->getFlashdata('success')) : ?>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->
    <!-- <script src="path_to_sweetalert2_js"></script> -->
    <script>
        // Tampilkan pesan sukses menggunakan SweetAlert2
        Swal.fire({
            icon: 'success',
            title: 'Berhasil!',
            text: "<?php echo session()->getFlashdata('success'); ?>"
        });
    </script>
<?php endif; ?>
<!-- App Header -->
<!-- <div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas  fa-2x">Terapi</i>
        </a>
    </div>
</div> -->
<!-- <div class="pageTitle">Blank Page</div>
<div class="right"></div> -->
<!-- * App Header -->

<!-- App Capsule -->
<div id="appCapsule">

    <div class="container">

        <div class="section full mt-2">
            <!-- <div class="section-title">Title</div> -->
            <div class="card mt-3 mb-5">

                <div class="wide-block pt-2 pb-2">


                    <div>
                        <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:60px;" class="rounded-pill">
                        Selamat Datang, <?= $data->namapanti ?>
                        <!-- <a href="/newterapi" class="btn btn-primary">Tambah Terapi</a> -->
                    </div>

                    <div class="card mt-2 mb-5">
                        <div class="card-header">
                            STATISTIK
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="row">
                                    <div class="col-1">
                                        <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:40px;" class="rounded-pill">
                                    </div>
                                    <div class="col">
                                        Jumlah Semua Lansia<br> <?= $lansia ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card mt-2">
                                <div class="row">
                                    <div class="col-1">
                                        <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:40px;" class="rounded-pill">
                                    </div>
                                    <div class="col">
                                        Jumlah Lansia Berhasil Terapi<br> 13
                                    </div>
                                </div>
                            </div>
                            <div class="card mt-2">
                                <div class="row">
                                    <div class="col-1">
                                        <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:40px;" class="rounded-pill">
                                    </div>
                                    <div class="col">
                                        Jumlah Lansia Tahap Terapi<br> 13
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
            <!-- <div class="card mt-2">
        ayo
    </div> -->
        </div>
    </div>
    <!-- * App Capsule -->

    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
        // new DataTable('#example');
    </script>
    <?= $this->endSection() ?>
<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>
<!-- loader -->
<div id="loader">
    <div class="spinner-border text-primary" role="status"></div>
</div>
<!-- * loader -->

<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas  fa-2x">Profil</i>
        </a>
    </div>
    <!-- <div class="pageTitle">Blank Page</div>
    <div class="right"></div> -->
</div>
<!-- * App Header -->

<!-- App Capsule -->
<div id="appCapsule">


    <div class="container mb-5">
        <div class="section full mt-4">
            <div class="section-title">Title</div>
            <div class="wide-block pt-2 pb-2">
                <div class="card mt-3 mb-5">
                    <div class="card-header">
                        Profil Pengguna
                        <!-- <p>Your email: <?= $user->alamat; ?></p>
                        <p>Hello, <?= session()->get('user')->id; ?></p> -->
                    </div>
                    <div class="card-body">

                        <form action="<?= base_url('/updateProfil') ?>" method="POST">
                            <div class="row center">
                                <div class="col-md-3">
                                    <label for="namapanti" class="col-form-label">Nama Panti</label>
                                </div>
                                <div class="col">
                                    <input type="text" id="namapanti" value="<?= $user->namapanti; ?>" class="form-control" name="namapanti">
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="namaketuapanti" class="col-form-label">Nama Ketua Panti</label>
                                </div>
                                <div class="col">
                                    <input type="text" id="namaketuapanti" class="form-control" name="namaketuapanti" value="<?= $user->namaketuapanti; ?>">
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="namaterapis" class="col-form-label">Nama Terapis</label>
                                </div>
                                <div class="col">
                                    <input type="text" id="namaterapis" class="form-control" name="namaterapis" value="<?= $user->namaterapis; ?>">
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="alamat" class="col-form-label">Alamat</label>
                                </div>
                                <div class="col">
                                    <input type="text" id="alamat" class="form-control" name="alamat" value="<?= $user->alamat; ?>">
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="nomorhp" class="col-form-label">Nomor</label>
                                </div>
                                <div class="col">
                                    <input type="text" id="nomorhp" class="form-control" name="nomorhp" value="<?= $user->nomerhp; ?>">
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="email" class="col-form-label">E-mail</label>
                                </div>
                                <div class="col">
                                    <input type="text" id="email" class="form-control" name="email" value="<?= $user->email; ?>">
                                </div>
                            </div>

                            <div class=" mt-2"></div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-between">
                                    <a href="/tambahdata" type="button" class="btn btn-secondary btn-sm">Kembali</a>
                                    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                                </div>
                            </div>
                        </form>
                        <div class="row center mt-2">
                            <!-- <div class="col-md-3 ">
                                <label for="username" class="col-form-label">Username</label>
                            </div>
                            <div class="col">
                                <input type="text" id="username" class="form-control" name="username" value="">
                            </div>
                        </div> -->
                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="password" class="col-form-label">Password</label>
                                </div>
                                <div class="col">
                                    <input type="password" id="password" class="form-control" name="password" value="<?= base64_encode($user->password); ?>">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-sm">Ganti</button>

                    </div>
                    <div class="mb-3">

                    </div>
                </div>
            </div>

        </div>
        <!-- <div class="card mt-2">
        ayo
    </div> -->

    </div>
    <!-- * App Capsule -->

    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
        // new DataTable('#example');
    </script>
    <?= $this->endSection() ?>
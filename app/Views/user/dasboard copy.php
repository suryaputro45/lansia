<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>

<?php if (session()->getFlashdata('success')) : ?>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->
    <script src="path_to_sweetalert2_js"></script>
    <script>
        // Tampilkan pesan sukses menggunakan SweetAlert2
        Swal.fire({
            icon: 'success',
            title: 'Berhasil!',
            text: "<?php echo session()->getFlashdata('success'); ?>"
        });
    </script>
<?php endif; ?>
<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas  fa-2x">Terapi</i>
        </a>
    </div>
    <!-- <div class="pageTitle">Blank Page</div>
    <div class="right"></div> -->
</div>
<!-- * App Header -->

<!-- App Capsule -->
<div id="appCapsule">

    <div class="container">

        <div class="section full mt-2">
            <div class="section-title">Title</div>
            <div class="card mt-3 mb-5">

                <div class="wide-block pt-2 pb-2">


                    <div>
                        <a href="/newterapi" class="btn btn-primary">Tambah Terapi</a>
                    </div>
                    <div class="mt-3 table-responsive mb-5">
                        <table class="table" id="myTable">
                            <thead>
                                <td>NO</td>
                                <td>Nama Pasien</td>
                                <td>Nama Terapis</td>
                                <td>Status</td>
                                <td>Aksi</td>
                            </thead>

                            <tbody>
                                <?php $no = 1;
                                foreach ($data as $val) :
                                ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $val['nama'] ?></td>
                                        <td><?= $val['namaterapi'] ?></td>
                                        <td><?= $val['status'] ?></td>
                                        <td><button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#edit"> Edit</button>
                                            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#hapus"> Hapus</button>
                                        </td>
                                    </tr>
                                <?php endforeach  ?>
                            </tbody>
                        </table>
                    </div>
                </div>



            </div>
            <!-- <div class="card mt-2">
        ayo
    </div> -->
        </div>
    </div>
    <!-- * App Capsule -->

    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
        // new DataTable('#example');
    </script>
    <?= $this->endSection() ?>
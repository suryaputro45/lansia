<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>
<!-- loader -->
<div id="loader">
    <div class="spinner-border text-primary" role="status"></div>
</div>
<!-- * loader -->
<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas  fa-2x">Terapi</i>
        </a>
    </div>
</div>
<div class="pageTitle">Blank Page</div>
<div class="right"></div>
<!-- * App Header -->
<!-- App Capsule -->
<div id="appCapsule">

    <?php if (session()->get('errors')) : ?>
        <script src="path_to_sweetalert2_js"></script>
        <script>
            // Tampilkan pesan error menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Terjadi kesalahan pada inputan:',
                html: "<?php echo implode('<br>', session()->get('errors')) ?>"
            });
        </script>
    <?php endif; ?>
    <?php if (session()->getFlashdata('error')) : ?>
        <!-- <script src="path_to_sweetalert2_js"></script> -->
        <script>
            // Tampilkan pesan sukses menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Kode uniq tidak Valid',
                text: "<?php echo session()->getFlashdata('error'); ?>"
            });
        </script>
    <?php endif; ?>


    <div class="container">

        <div class="card">
            <div class="card-header">
                <h5 class="card-title" id="tambahdata">Edit Data Pasien</h5>
                <button type="button" class="close" data-dismiss="card" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-body">
                <form action="<?= base_url('/updatedata') ?>" method="POST">
                    <input type="hidden" id="idprofil" value="" class="form-control" name="idprofil">

                    <div class="row center">
                        <div class="col-md-3">
                            <label for="namapasien" class="col-form-label">Nama Pasien</label>
                        </div>
                        <div class="col">
                            <input type="text" id="namapasien" value="" class="form-control" name="namapasien">
                        </div>
                    </div>
                    <div class="row center mt-2">
                        <div class="col-md-3 ">
                            <label for="alamatpasien" class="col-form-label">Alamat Pasien</label>
                        </div>
                        <div class="col">
                            <input type="text" id="alamatpasien" class="form-control" name="alamatpasien" value="">
                        </div>
                    </div>
                    <div class="row center mt-2">
                        <div class="col-md-3 ">
                            <label for="telppasien" class="col-form-label">Nomor Hp</label>
                        </div>
                        <div class="col">
                            <input type="text" id="telppasien" class="form-control" name="telppasien" value="">
                        </div>
                    </div>
                    <div class=" mt-2"></div>
            </div>
            <div class="card-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
    <?= $this->endSection() ?>
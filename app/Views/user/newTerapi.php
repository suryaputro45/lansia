<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>
<link href="<?= base_url('/') ?>assets/bootstrap/select2/select2.min.css" rel="stylesheet" />
<script src="<?= base_url('/') ?>assets/bootstrap/select2/select2.min.js"></script>
<!-- loader -->
<div id="loader">
    <div class="spinner-border text-primary" role="status"></div>
</div>
<!-- * loader -->

<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas  fa-2x">Tambah Terapi</i>
        </a>
    </div>
    <!-- <div class="pageTitle">Blank Page</div>
    <div class="right"></div> -->
</div>
<!-- * App Header -->

<!-- App Capsule -->
<div id="appCapsule">


    <div class="container">
        <div class="section full mt-4 mb-5">
            <div class="section-title">Title</div>
            <div class="wide-block pt-2 pb-2">
                <!-- <div>
                    <button class="btn btn-primary">Kembali</button>
                </div> -->
                <div class="card mt-3 mb-5">
                    <div class="card-header">
                        Silahkan Data Yang Akan Diterapi
                    </div>
                    <div class="card-body">

                        <form action="<?= base_url('/saveterapi') ?>" method="POST" enctype="multipart/form-data">
                            <div class="row center">
                                <div class="col-md-3">
                                    <label for="namapasien" class="col-form-label">Nama Pasien</label>
                                </div>
                                <div class="col">
                                    <!-- <input type="text" id="namapasien" value="<?= old('namapasien') ?>" class="form-control" name="namapasien"> -->

                                    <select class="form-control" name="namapasien" id="namapasien">
                                        <?php foreach ($data as $val) : ?>
                                            <option value="">--pilih pasien--</option>
                                            <option value="<?= $val['idpasien'] ?>"><?= $val['nama'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                            </div>

                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="alamatpasien" class="col-form-label">Nama Terapi</label>
                                </div>
                                <div class="col">
                                    <select class="form-control" name="namaterapi" id="namaterapi">
                                        <?php foreach ($terapi as $val) : ?>
                                            <option value="">--pilih pasien--</option>
                                            <option value="<?= $val['idterapi'] ?>"><?= $val['namaterapi'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="telppasien" class="col-form-label">Status Terapi</label>
                                </div>
                                <div class="col">
                                    <select class="form-control" name="statusterapi" id="statusterapi">
                                        <option value="">--Pilih Status--</option>
                                        <?php foreach ($status as $val) : ?>

                                            <option value="<?= $val['idstatus'] ?>"><?= $val['namastatus'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="telppasien" class="col-form-label">Pilih Berkas</label>
                                </div>
                                <div class="col-sm-2">
                                    <img src="/assets/images/" class="img-thumbnail img-preview" alt="berkas.pdf">
                                </div>
                                <div class="col-sm-7">
                                    <input class="form-control" type="file" id="document1" name="document1">
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="telppasien" class="col-form-label">Pilih Berkas</label>
                                </div>
                                <div class="col-sm-2">
                                    <img src="/assets/images/" class="img-thumbnail img-preview2" alt="berkas.pdf">
                                </div>
                                <div class="col-sm-7">
                                    <input class="form-control" type="file" id="document2" name="document2" value="" onchange="previewImg2()">
                                </div>
                            </div>
                    </div>

                    <div class=" mt-2"></div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-between">
                            <a href="/user" type="button" class="btn btn-secondary btn-sm">Kembali</a>
                            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                        </div>
                    </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

</div>
<!-- <div class="card mt-2">
        ayo
    </div> -->

</div>
<!-- * App Capsule -->
<script>
    function previewImg() {
        const urlGambar = document.querySelector('#document1');
        const imgPreview = document.querySelector('.img-preview');

        const fileGambar = urlGambar.files[0];

        const reader = new FileReader();

        reader.onload = function(e) {
            // Mengambil alamat gambar (URL data) dari file yang dipilih
            const urlGambarData = e.target.result;

            // Menampilkan pratinjau gambar di elemen img dengan class 'img-preview'
            imgPreview.src = urlGambarData;

            // Menggunakan urlGambarData sesuai kebutuhan Anda (misalnya: menyimpan di database atau mengirimkan ke server)
            // console.log('Alamat gambar: ', urlGambarData);
        }

        // Membaca file gambar sebagai URL data
        reader.readAsDataURL(fileGambar);
    }

    function previewImg2() {
        const urlGambar = document.querySelector('#document2');
        const imgPreview = document.querySelector('.img-preview2');

        const fileGambar = urlGambar.files[0];

        const reader = new FileReader();

        reader.onload = function(e) {
            // Mengambil alamat gambar (URL data) dari file yang dipilih
            const urlGambarData = e.target.result;

            // Menampilkan pratinjau gambar di elemen img dengan class 'img-preview'
            imgPreview.src = urlGambarData;

            // Menggunakan urlGambarData sesuai kebutuhan Anda (misalnya: menyimpan di database atau mengirimkan ke server)
            // console.log('Alamat gambar: ', urlGambarData);
        }

        // Membaca file gambar sebagai URL data
        reader.readAsDataURL(fileGambar);
    }
</script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        // $('.js-example-basic-multiple').select2({
        //     placeholder: 'Masukan Nama Pasien',
        //     minimumInputLength: 2,
        //     minimumResultsForSearch: 2
        // });

    });
    $("#namapasien").select2({
        placeholder: "Masukan Nama Pasien",
        allowClear: true,
        minimumInputLength: 2,
        // minimumResultsForSearch: 2
    });
    $("#namaterapi").select2({
        placeholder: "Masukan Nama Terapi",
        allowClear: true,
        minimumInputLength: 2,
        // minimumResultsForSearch: 2
    });
    // new DataTable('#example');
</script>
<?= $this->endSection() ?>
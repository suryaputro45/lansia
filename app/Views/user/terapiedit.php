<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>
<!-- loader -->
<div id="loader">
    <div class="spinner-border text-primary" role="status"></div>
</div>
<!-- * loader -->

<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <!-- <i class="fas  fa-2x"></i> -->
            <i class="fas fa-arrow-left fa-2x"></i>
        </a>
    </div>
    <!-- <div class="pageTitle">Blank Page</div>
    <div class="right"></div> -->
</div>
<!-- * App Header -->

<!-- App Capsule -->
<div id="appCapsule">

    <?php if (session()->get('errors')) : ?>
        <script src="path_to_sweetalert2_js"></script>
        <script>
            // Tampilkan pesan error menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Terjadi kesalahan pada inputan:',
                html: "<?php echo implode('<br>', session()->get('errors')) ?>"
            });
        </script>
    <?php endif; ?>
    <?php if (session()->getFlashdata('error')) : ?>
        <!-- <script src="path_to_sweetalert2_js"></script> -->
        <script>
            // Tampilkan pesan sukses menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Kode uniq tidak Valid',
                text: "<?php echo session()->getFlashdata('error'); ?>"
            });
        </script>
    <?php endif; ?>
    <div class="container">
        <div class="section full mt-4 mb-5">
            <div class="section-title">Title</div>
            <div class="wide-block pt-2 pb-2">
                <!-- <div>
                    <button class="btn btn-primary">Kembali</button>
                </div> -->
                <div class="card mt-3 mb-5">
                    <div class="card-header">
                        <h5>Edit Data Terapi</h5>
                    </div>
                    <div class="card-body">

                        <form action="<?= base_url('/updatekategori') ?>" method="POST" enctype="multipart/form-data">
                            <?php // dd($data);
                            foreach ($data as $val) : ?>
                                <div class="row center">
                                    <div class="col">
                                        <input type="text" id="idterapi" value="<?= $val['idterapi'] ?>" class="form-control" name="idterapi" placeholder="Id Terapi">
                                    </div>
                                </div>

                                <div class="row center mt-2">

                                    <div class="col">
                                        <input type="text" id="hariterapi" class="form-control" name="hariterapi" value="<?= $val['hariterapi'] ?>" placeholder="Hari Terapi">
                                    </div>

                                </div>
                                <div class="row center mt-2">

                                    <div class="col">
                                        <input type="text" id="namaterapi" class="form-control" name="namaterapi" value="<?= $val['namaterapi'] ?>" placeholder="Nama Terapi">
                                    </div>


                                </div>

                                <div class="row center mt-2">

                                    <div class="col">
                                        <input type="text" id="linkyt" class="form-control" name="linkyt" value="<?= $val['linkyt'] ?>" placeholder="Link Video Youtube">
                                    </div>
                                </div>
                                <div class="row center mt-2">

                                    <div class="col">
                                        <input type="text" id="fotokegiatan" class="form-control" name="fotokegiatan" value="" placeholder="Foto Terapi" readonly>
                                    </div>

                                    <div class=" col">
                                        <input type="file" id="fototerapi" class="form-control" name="fototerapi" value="">
                                    </div>
                                </div>
                                <div class="row center mt-2">
                                    <div class="col">
                                        <textarea class="form-control" placeholder="Deskripsi Terapi" id="deskripsi" name="deskripsi"><?= $val['deskripsiterapi'] ?></textarea>
                                    </div>
                                </div>

                                <div class=" mt-2"></div>
                                <div class="card-footer">
                                    <div class="d-flex justify-content-between">
                                        <a href="<?= base_url('/') ?>dataterapi" type="button" class="btn btn-secondary btn-sm">Kembali</a>
                                        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- <div class="card mt-2">
        ayo
    </div> -->

</div>
<!-- * App Capsule -->
<script>
    function previewImg() {
        const inputGambar = document.querySelector('#fotolansia');
        const imgPreview = document.querySelector('.img-preview');

        if (inputGambar.files && inputGambar.files[0]) {
            const fileGambar = inputGambar.files[0];
            const validImageTypes = ['image/jpeg', 'image/png', 'image/gif']; // Tipe file gambar yang diizinkan

            if (validImageTypes.includes(fileGambar.type)) {
                const reader = new FileReader();

                reader.onload = function(e) {
                    const urlGambarData = e.target.result;
                    imgPreview.src = urlGambarData;
                    // Anda dapat melakukan tindakan lain di sini jika perlu
                }

                reader.readAsDataURL(fileGambar);
            } else {
                alert('Pilih file gambar yang valid (JPEG, PNG, GIF).');
                inputGambar.value = ''; // Menghapus nilai input file jika tidak valid
                imgPreview.src = ''; // Menghapus pratinjau gambar
            }
        } else {
            imgPreview.src = ''; // Menghapus pratinjau gambar jika tidak ada file yang dipilih
        }
    }
</script>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
    // new DataTable('#example');
</script>
<?= $this->endSection() ?>
<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>
<div id="loader">
    <div class="spinner-border text-primary" role="status"></div>
</div>
<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas fa-arrow-left fa-2x"></i>
        </a>
    </div>
</div>
<div class="contatainer">
    <div class="section full mt-4 mb-5">
        <div class="section-title">Title</div>
        <div class="wide-block pt-2 pb-2">

            <div class="card mt-2">
                <h1><?= $judul ?></h1>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="<?= $video ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                    <!-- <iframe width="640" height="360" src="https://www.youtube.com/embed/VIDEO_ID" frameborder="0" allowfullscreen></iframe> -->
                </div>
                <div class="card-footer">

                    <form action="<?= base_url('/') ?>simpanVidio" method="post">
                        <input type="hidden" name='kode' value="<?= $id ?>">
                        <input type="hidden" name='profil' value="<?= $idprofil ?>">
                        <div class="d-flex justify-content-between">
                            <a href="<?= base_url('/') ?>terapi" type="button" class="btn btn-secondary btn-sm">Kembali</a>
                            <button type="submit" class="btn btn-success btn-sm">Selesai</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?= $this->endSection() ?>
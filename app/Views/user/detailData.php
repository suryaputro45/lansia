<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>

<?php if (session()->getFlashdata('success')) : ?>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->
    <!-- <script src="path_to_sweetalert2_js"></script> -->
    <script>
        // Tampilkan pesan sukses menggunakan SweetAlert2
        Swal.fire({
            icon: 'success',
            title: 'Berhasil!',
            text: "<?php echo session()->getFlashdata('success'); ?>"
        });
    </script>
<?php endif; ?>
<?php if (session()->get('errors')) : ?>

    <script>
        // Tampilkan pesan error menggunakan SweetAlert2
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Data sebelumya Belum selesai',
            html: "<?php echo session()->get('errors') ?>"
        });
    </script>
<?php endif; ?>
<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas fa-arrow-left fa-2x"></i>
        </a>
    </div>
</div>
<!-- <div class="pageTitle">Blank Page</div>
<div class="right"></div> -->
<!-- * App Header -->

<!-- App Capsule -->
<div id="appCapsule" class="mt-4">

    <div class="container mt-5">

        <div class="section full mt-5">
            <!-- <div class="section-title">Title</div> -->
            <div class="card mt-5 mb-5">

                <div class="wide-block pt-2 pb-2">

                    <div>
                        <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:60px;" class="rounded-pill">
                        <?= strtoupper($data[0]['nama']) ?>
                        <!-- <a href="/newterapi" class="btn btn-primary">Tambah Terapi</a> -->
                    </div>

                    <div class="card mt-2 mb-5">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <a href="<?= base_url('/') ?>tampildata/<?= $data[0]['idprofil'] ?>" type="button" class="btn btn-secondary btn-sm">Tampil Data</a>
                                <!-- <button type="submit" class="btn btn-success btn-sm">Tambah Catatan</button> -->
                                <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#tambahdata"> Tambah Catatan</button>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- <?php // dd($terapi) 
                                    ?> -->
                            <div class="">
                                <form action="#" method="post" id="myForm">

                                    <?php

                                    // if ($trasaksi == null) {
                                    $no = 0;
                                    foreach ($terapi as $val) : ?>
                                        <!-- Kolom Awal -->
                                        <div class="card mt-2">
                                            <div class="row">
                                                <div class="col-2">
                                                    <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:40px;" class="rounded-pill">
                                                </div>
                                                <div class="col">
                                                    <a href="<?= base_url('/vidio' . '/' . $val['idterapi'] . '/' . $data[0]['idprofil']) ?>"> Hari <?= $val['hariterapi']  ?> : <?= $val['namaterapi']  ?></a>
                                                </div>

                                                <div class="col-2 d-flex flex-column align-items-start justify-content-center">
                                                    <?php //echo $no++;
                                                    if (isset($trasaksi[$no]) && isset($trasaksi[$no]['status'])) {
                                                        if ($trasaksi[$no]['status'] == 1) {
                                                            // Tindakan jika status = 1
                                                            echo '<i class="fas fa-check-circle text-success"></i>';
                                                        } else {
                                                            // Tindakan jika status bukan 1
                                                            echo '<i class="fas fa-times-circle text-danger"></i>';
                                                        }
                                                    } else {
                                                        // Tindakan jika indeks atau 'status' tidak ada dalam array
                                                        echo '<i class="fas fa-times-circle text-danger"></i>';
                                                    }
                                                    $no++;
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>

                                </form>
                            </div>
                            <br>
                            <!-- <button type="button" class="btn btn-primary btn-sm" id="addRow">Tambah Baris</button> -->

                            <script>
                                $(document).ready(function() {
                                    // Tangkap tombol "Add Row" menggunakan jQuery
                                    $('#addRow').click(function() {
                                        // Clone elemen row pertama
                                        var newRow = $('.row:first').clone();

                                        // Hapus nilai input pada baris baru
                                        newRow.find('input').val('');

                                        // Tambahkan baris baru ke formulir
                                        $('#myForm').append(newRow);
                                    });
                                });
                            </script>
                            <!-- <div class="card mt-2">
                                <div class="row">
                                    <div class="col-1">
                                        <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:40px;" class="rounded-pill">
                                    </div>
                                    <div class="col">
                                        Jumlah semua<br> 13
                                    </div>
                                </div>
                            </div>
                            <div class="card mt-2">
                                <div class="row">
                                    <div class="col-1">
                                        <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:40px;" class="rounded-pill">
                                    </div>
                                    <div class="col">
                                        Jumlah semua<br> 13
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>

            </div>
            <!-- <div class="card mt-2">
            ayo
            </div> -->
        </div>
    </div>
    <!-- * App Capsule -->
    <!-- Modal -->
    <div class="modal fade" id="tambahdata" tabindex="-1" role="dialog" aria-labelledby="tambahdata" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahdata">Edit Data Pasien</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('/updatedata') ?>" method="POST">
                        <input type="hidden" id="idprofil" value="" class="form-control" name="idprofil">

                        <div class="row center">
                            <div class="col-md-3">
                                <label for="namapasien" class="col-form-label">Nama Pasien</label>
                            </div>
                            <div class="col">
                                <input type="text" id="namapasien" value="" class="form-control" name="namapasien">
                            </div>
                        </div>
                        <div class="row center mt-2">
                            <div class="col-md-3 ">
                                <label for="alamatpasien" class="col-form-label">Alamat Pasien</label>
                            </div>
                            <div class="col">
                                <input type="text" id="alamatpasien" class="form-control" name="alamatpasien" value="">
                            </div>
                        </div>
                        <div class="row center mt-2">
                            <div class="col-md-3 ">
                                <label for="telppasien" class="col-form-label">Nomor Hp</label>
                            </div>
                            <div class="col">
                                <input type="text" id="telppasien" class="form-control" name="telppasien" value="">
                            </div>
                        </div>
                        <div class=" mt-2"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
        // new DataTable('#example');
    </script>
    <?= $this->endSection() ?>
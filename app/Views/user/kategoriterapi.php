<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>

<?php if (session()->getFlashdata('success')) : ?>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->
    <!-- <script src="path_to_sweetalert2_js"></script> -->
    <script>
        // Tampilkan pesan sukses menggunakan SweetAlert2
        Swal.fire({
            icon: 'success',
            title: 'Berhasil!',
            text: "<?php echo session()->getFlashdata('success'); ?>"
        });
    </script>
<?php endif; ?>
<?php if (session()->get('errors')) : ?>

    <script>
        // Tampilkan pesan error menggunakan SweetAlert2
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Data sebelumya Belum selesai',
            html: "<?php echo session()->get('errors') ?>"
        });
    </script>
<?php endif; ?>
<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas fa-arrow-left fa-2x"></i>
        </a>
    </div>
</div>
<!-- <div class="pageTitle">Blank Page</div>
<div class="right"></div> -->
<!-- * App Header -->

<!-- App Capsule -->
<div id="appCapsule" class="mt-4">

    <div class="container mt-5">

        <div class="section full mt-5">
            <!-- <div class="section-title">Title</div> -->
            <div class="container mt-5">

                <div class="section full mt-5">
                    <!-- <div class="section-title">Title</div> -->
                    <div class="card mt-5 mb-5">

                        <div class="wide-block pt-2 pb-2">

                            <div>
                                <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:60px;" class="rounded-pill">

                                <!-- <a href="/newterapi" class="btn btn-primary">Tambah Terapi</a> -->
                            </div>

                            <div class="card mt-2 mb-5">
                                <div class="card-header">
                                    <div class="d-flex justify-content-between">
                                        <!-- <button type="submit" class="btn btn-success btn-sm">Tambah Catatan</button> -->
                                        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#tambahdata"> Tambah Catatan</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!-- <?php // dd($terapi) 
                                            ?> -->
                                    <div class="">
                                        <form action="#" method="post" id="myForm">

                                            <?php

                                            // if ($trasaksi == null) {
                                            $no = 0;
                                            foreach ($terapi as $val) : ?>
                                                <!-- Kolom Awal -->
                                                <div class="card mt-2">
                                                    <div class="row">
                                                        <div class="col-2">
                                                            <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:40px;" class="rounded-pill">
                                                        </div>
                                                        <div class="col">
                                                            Hari <?= $val['hariterapi']  ?> : <?= $val['namaterapi']  ?>
                                                        </div>

                                                        <div class="col-2 d-flex flex-column align-items-start justify-content-center">
                                                            <a href="<?= base_url('/updateterapi') . '/' . $val['idterapi'] ?>"><i class="fas fa-edit"></i></a>

                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>

                                        </form>
                                    </div>
                                    <br>
                                    <!-- <button type="button" class="btn btn-primary btn-sm" id="addRow">Tambah Baris</button> -->

                                    <script>
                                        $(document).ready(function() {
                                            // Tangkap tombol "Add Row" menggunakan jQuery
                                            $('#addRow').click(function() {
                                                // Clone elemen row pertama
                                                var newRow = $('.row:first').clone();

                                                // Hapus nilai input pada baris baru
                                                newRow.find('input').val('');

                                                // Tambahkan baris baru ke formulir
                                                $('#myForm').append(newRow);
                                            });
                                        });
                                    </script>
                                    <!-- <div class="card mt-2">
                                <div class="row">
                                    <div class="col-1">
                                        <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:40px;" class="rounded-pill">
                                    </div>
                                    <div class="col">
                                        Jumlah semua<br> 13
                                    </div>
                                </div>
                            </div>
                            <div class="card mt-2">
                                <div class="row">
                                    <div class="col-1">
                                        <img src="<?= base_url('/assets/images/default.jpg') ?>" alt="Avatar Logo" style="width:40px;" class="rounded-pill">
                                    </div>
                                    <div class="col">
                                        Jumlah semua<br> 13
                                    </div>
                                </div>
                            </div> -->
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- <div class="card mt-2">
            ayo
            </div> -->
                </div>
            </div>
            <!-- <div class="card mt-2">
            ayo
            </div> -->
        </div>
    </div>
    <!-- * App Capsule -->
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
        // new DataTable('#example');
    </script>
    <?= $this->endSection() ?>
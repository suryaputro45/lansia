<?= $this->extend('tamplate/layoutuser') ?>

<?= $this->section('isi') ?>
<!-- loader -->
<div id="loader">
    <div class="spinner-border text-primary" role="status"></div>
</div>
<!-- * loader -->

<!-- App Header -->
<div class="appHeader bg-primary text-light">
    <div class="left">
        <a href="javascript:;" class="headerButton goBack">
            <i class="fas  fa-2x">Tambah Pasien</i>
        </a>
    </div>
    <!-- <div class="pageTitle">Blank Page</div>
    <div class="right"></div> -->
</div>
<!-- * App Header -->

<!-- App Capsule -->
<div id="appCapsule">

    <?php if (session()->get('errors')) : ?>
        <script src="path_to_sweetalert2_js"></script>
        <script>
            // Tampilkan pesan error menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Terjadi kesalahan pada inputan:',
                html: "<?php echo implode('<br>', session()->get('errors')) ?>"
            });
        </script>
    <?php endif; ?>
    <?php if (session()->getFlashdata('error')) : ?>
        <!-- <script src="path_to_sweetalert2_js"></script> -->
        <script>
            // Tampilkan pesan sukses menggunakan SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Kode uniq tidak Valid',
                text: "<?php echo session()->getFlashdata('error'); ?>"
            });
        </script>
    <?php endif; ?>
    <div class="container">
        <div class="section full mt-4 mb-5">
            <div class="section-title">Title</div>
            <div class="wide-block pt-2 pb-2">
                <!-- <div>
                    <button class="btn btn-primary">Kembali</button>
                </div> -->
                <div class="card mt-3 mb-5">
                    <div class="card-header">
                        Silahkan Masukan data Pasien
                    </div>
                    <div class="card-body">

                        <form action="<?= base_url('/save') ?>" method="POST" enctype="multipart/form-data">
                            <div class="row center">
                                <div class="col">
                                    <input type="text" id="namapasien" value="<?= old('namapasien') ?>" class="form-control" name="namapasien" placeholder="Nama Lengkap Lansia">
                                </div>
                            </div>
                            <div class="row center mt-2">

                                <div class="col">
                                    <input class="form-control" type="file" id="fotolansia" name="fotolansia" value="">
                                </div>
                                <!-- </div> -->
                            </div>
                            <div class="row center mt-2">

                                <div class="col">
                                    <input type="text" id="tempatlahir" class="form-control" name="tempatlahir" value="<?= old('tempatlahir') ?>" placeholder="Tempat Lahir">
                                </div>
                                <div class="col">
                                    <input type="date" id="tanggallahir" class="form-control" name="tanggallahir" value="<?= old('tanggallahir') ?>" placeholder="Tanggal Lahir">
                                </div>
                            </div>
                            <div class="row center mt-2">

                                <div class="col">
                                    <input type="text" id="alamatpasien" class="form-control" name="alamatpasien" value="<?= old('alamatpasien') ?>" placeholder="Alamat Rumah Asal">
                                </div>
                                <div class="col">
                                    <input type="file" id="fotorumah" class="form-control" name="fotorumah" value="<?= old('fotorumah') ?>">

                                </div>

                            </div>

                            <div class="row center mt-2">

                                <div class="col">
                                    <input type="text" id="namapasangan" class="form-control" name="namapasangan" value="<?= old('namapasangan') ?>" placeholder="Nama Suami/Istri">
                                </div>
                                <div class="col">
                                    <input type="file" id="fotopasangan" class="form-control" name="fotopasangan" value="<?= old('fotopasangan') ?>" placeholder="ja">

                                </div>
                            </div>
                            <div class="row center mt-2">
                                <!-- <div class="col-md-3 ">
                                    <label for="alamatpasien" class="col-form-label">Alamat Pasien</label>
                                </div> -->
                                <div class="col">
                                    <input type="text" id="namaanak" class="form-control" name="namaanak" value="<?= old('namaanak') ?>" placeholder="Nama Anak">
                                </div>
                                <div class="col">
                                    <input type="file" id="fotoanak" class="form-control" name="fotoanak" value="<?= old('fotoanak') ?>">
                                    <!-- <label for="fotoanak" style="display: block; ">
                                        <i class="fas fa-camera" style="font-size: 32px; margin-right: 8px;"></i>
                                        Foto Anak
                                    </label> -->
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <!-- <div class="col-md-3 ">
                                    <label for="alamatpasien" class="col-form-label">Alamat Pasien</label>
                                </div> -->
                                <div class="col">
                                    <input type="text" id="namasaudara" class="form-control" name="namasaudara" value="<?= old('namasaudara') ?>" placeholder="Nama Sanak Saudara">
                                </div>
                                <div class="col">
                                    <input type="file" id="fotosaudara" class="form-control" name="fotosaudara" value="<?= old('fotosaudara') ?>">
                                    <!-- <label for="fotosanaksaudara" style="display: block; ">
                                        <i class="fas fa-camera" style="font-size: 32px; margin-right: 8px;"></i>
                                        Foto Sanak Saudara
                                    </label> -->
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <!-- <div class="col-md-3 ">
                                    <label for="alamatpasien" class="col-form-label">Alamat Pasien</label>
                                </div> -->
                                <div class="col">
                                    <input type="text" id="namaingattempat" class="form-control" name="namaingattempat" value="<?= old('namaingattempat') ?>" placeholder="Nama Tempat Paling Diingat">
                                </div>
                                <div class="col">
                                    <input type="file" id="fotoingatempat" class="form-control" name="fotoingatempat" value="<?= old('fotoingatempat') ?>">
                                    <!-- <label for="fotoingatempat" style="display: block; ">
                                        <i class="fas fa-camera" style="font-size: 32px; margin-right: 8px;"></i>
                                        Foto Tempat
                                    </label> -->
                                </div>
                            </div>
                            <div class="row center mt-2">
                                <div class="col-md-3 ">
                                    <label for="telppasien" class="col-form-label">Nomor Hp</label>
                                </div>
                                <div class="col">
                                    <input type="text" id="telppasien" class="form-control" name="telppasien" value="<?= old('telppasien') ?>">
                                </div>
                            </div>
                            <div class=" mt-2"></div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-between">
                                    <a href="<?= base_url('/') ?>terapi" type="button" class="btn btn-secondary btn-sm">Kembali</a>
                                    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- <div class="card mt-2">
        ayo
    </div> -->

</div>
<!-- * App Capsule -->
<script>
    function previewImg() {
        const inputGambar = document.querySelector('#fotolansia');
        const imgPreview = document.querySelector('.img-preview');

        if (inputGambar.files && inputGambar.files[0]) {
            const fileGambar = inputGambar.files[0];
            const validImageTypes = ['image/jpeg', 'image/png', 'image/gif']; // Tipe file gambar yang diizinkan

            if (validImageTypes.includes(fileGambar.type)) {
                const reader = new FileReader();

                reader.onload = function(e) {
                    const urlGambarData = e.target.result;
                    imgPreview.src = urlGambarData;
                    // Anda dapat melakukan tindakan lain di sini jika perlu
                }

                reader.readAsDataURL(fileGambar);
            } else {
                alert('Pilih file gambar yang valid (JPEG, PNG, GIF).');
                inputGambar.value = ''; // Menghapus nilai input file jika tidak valid
                imgPreview.src = ''; // Menghapus pratinjau gambar
            }
        } else {
            imgPreview.src = ''; // Menghapus pratinjau gambar jika tidak ada file yang dipilih
        }
    }
</script>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
    // new DataTable('#example');
</script>
<?= $this->endSection() ?>
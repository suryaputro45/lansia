<?php

namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Exceptions\PageNotFoundException;

class Penomoran extends Model
{
    // public $table;
    public function getNomorBaru($table = null, $cari = [], $field = null)
    {

        $query = $this->db->table($table)
            ->selectMax($field)
            ->where($cari)
            ->get();
        $result = $query->getRow();

        $maxId = $result->$field ?? 0;
        $idBaru = $maxId + 1;

        return $idBaru;
    }
    public function getData($field = null, $table = null, $cari = [],  $orderBy = null, $short = null)
    {

        $query = $this->db->table($table)
            ->select($field)
            ->where($cari)
            ->orderBy($orderBy, $short)
            ->get();
        $result = $query->getResultArray();

        // $maxId = $result->$field ?? 0;
        // $idBaru = $maxId + 1;
        // Check if any data was found
        // if (empty($result)) {
        // Menampilkan pesan peringatan dengan JavaScript
        // echo '<script>alert("Data tidak ditemukan");</script>';
        // \session()->setFlashdata('success', 'Kode Uniq anda tidak valid!');
        // return redirect()->back()->withInput();
        // Mengarahkan kembali ke halaman sebelumnya dengan JavaScript
        // echo '<script>window.history.back();</script>';

        // Menghentikan eksekusi kode selanjutnya
        // return;
        // }

        return $result;
    }
    public function getDataJoin($field = null, $table = null, $join1 = null, $join2 = null, $join3 = null, $joinon = null, $joinon2 = null, $joinon3 = null, $cari = [],  $orderBy = null, $short = null)
    {

        $query = $this->db->table($table)
            ->select($field)
            ->join($join1, $joinon)
            ->join($join2, $joinon2)
            ->join($join3, $joinon3)
            ->where($cari)
            ->orderBy($orderBy, $short)
            ->get();
        $result = $query->getResultArray();

        // $maxId = $result->$field ?? 0;
        // $idBaru = $maxId + 1;

        return $result;
    }
    public function getDataJoin2($field = null, $table = null, $join1 = null,  $joinon = null,  $cari = [],  $orderBy = null, $short = null)
    {

        $query = $this->db->table($table)
            ->select($field)
            ->join($join1, $joinon, 'left')

            ->where($cari)
            ->orderBy($orderBy, $short)
            ->get();
        $result = $query->getResultArray();

        // $maxId = $result->$field ?? 0;
        // $idBaru = $maxId + 1;

        return $result;
    }
}

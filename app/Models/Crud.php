<?php

namespace App\Models;

use CodeIgniter\Model;

class Crud extends Model
{
    public function tambahData($tabel, $data)
    {
        // $query = "INSERT INTO {$tabel} values {$data}";
        $result = $this->db->table($tabel)->insert($data); //$this->db->query($query);
        return $result;
    }
    // public $table;
    public function getSetting($field = null, $table = null, $cari = [],  $orderBy = null, $short = null)
    {

        $query = $this->db->table($table)
            ->select($field)
            ->where($cari)
            ->orderBy($orderBy, $short)
            ->get();
        $result = $query->getResultArray();

        // $maxId = $result->$field ?? 0;
        // $idBaru = $maxId + 1;

        return $result;
    }
    public function postSetting($field = null, $table = null, $cari = [],  $orderBy = null, $short = null)
    {

        $query = $this->db->table($table)
            ->select($field)
            ->where($cari)
            ->orderBy($orderBy, $short)
            ->get();
        $result = $query->getResultArray();

        // $maxId = $result->$field ?? 0;
        // $idBaru = $maxId + 1;

        return $result;
    }
    public function updateSetting($tabel, $data, $id)
    {
        $query = $this->db->table($tabel)
            ->where('idapp', $id)
            ->update($data);
        // dd($query);
        return $query;
    }
}

<?php

namespace App\Models\SuperAdmin;

use CodeIgniter\Model;

class Menu extends Model
{
    // public $table;
    public function getSemuaMenu($field = null, $table = null, $cari = [],  $orderBy = null, $short = null)
    {

        $query = $this->db->table($table)
            ->select($field)
            ->where($cari)
            ->orderBy($orderBy, $short)
            ->get();
        $result = $query->getResultArray();

        // $maxId = $result->$field ?? 0;
        // $idBaru = $maxId + 1;

        return $result;
    }
}

<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/login', 'Home::login');
$routes->get('/logout', 'Home::logout');
$routes->post('/ceklogin', 'Home::ceklogin');
$routes->get('/daftar', 'Home::daftar');
$routes->post('/proses', 'Home::proses');
$routes->post('/daftarpengguna', 'Home::daftarpengguna');
$routes->post('/superadmin', 'SuperAdmin\Dasboard::index');
$routes->get('/superadmin', 'SuperAdmin\Dasboard::index');
$routes->get('/user', 'User\Dasboard::index');
$routes->get('/profil', 'User\Profil::index');
$routes->get('/tambahdata', 'User\TambahData::index');
$routes->get('/newdata', 'User\TambahData::newData');
$routes->post('/save', 'User\TambahData::saveBaru');
$routes->post('/updatedata', 'User\TambahData::updateData');
$routes->post('/hapusData', 'User\TambahData::hapusData');
$routes->get('/newterapi', 'User\TambahData::newTerapi');
$routes->post('/saveterapi', 'User\TambahData::saveterapi');
$routes->post('/updateProfil', 'User\TambahData::updateProfil');
$routes->get('/detaildata/(:num)', 'User\TambahData::DetailPasien/$1');

$routes->get('/updateuser', 'Home::updateuser');

$routes->get('/vidio/(:num)/(:num)', 'User\Vidio::index/$1/$2');
$routes->post('/simpanVidio', 'User\Vidio::simpanVidio');
$routes->get('/tampildata/(:num)', 'User\TambahData::tampilData/$1');

//menu terapi admin
$routes->get('/terapi', 'User\Terapi::index');
$routes->get('/dataterapi', 'User\Terapi::dataterapi');
$routes->get('/updateterapi/(:num)', 'User\Terapi::updateTerapi/$1');



$routes->get('/admin', 'Admin\Dasboard::index');
$routes->get('/settingapp', 'Admin\Dasboard::settingapp');
//admin
$routes->post('/updateApp', 'Admin\Dasboard::updateApp');
// $routes->post('/updateApp', 'Admin\Dasboard::updateApp');

// $routes->get('auth/authorize', 'AuthController::authorize');
// $routes->get('/callback', 'AuthController::callback');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\Penomoran;
use App\Models\SuperAdmin\Menu;
use App\Models\Admin\SettingApp;


class Dasboard extends BaseController
{
    protected $penomoran;
    protected $menu;
    protected $settingApp;
    protected $validation;

    public function __construct()
    {
        helper('form');
        $this->penomoran = new Penomoran();
        $this->menu = new Menu();
        $this->settingApp = new SettingApp();
        $this->validation = \Config\Services::validation();
    }

    public function index()
    {
        $hasil  = $this->menu->getSemuaMenu('*', 'menu', ["status" => "1", "keterangan" => "submenu", "role" => 1], 'kdmenu', '');
        $judulmenu  = $this->menu->getSemuaMenu('*', 'menu', ["status" => "1", "target" => "0", "role" => 1], 'kdmenu', '');
        // var_dump($judulmenu);
        // die;
        $data = [
            'menu' => $hasil,
            'judul' => $judulmenu,
        ];
        // var_dump($hasil[0]['kdmenu']);
        // die;
        return view('Admin/dasboard', $data);
    }
    public function settingapp()
    {
        $hasil = $this->settingApp->getSetting('*', 'app', ["statusapp" => 1], '', '');
        $data = [
            'hasil' => $hasil,
        ];
        // var_dump($hasil[0]['kdmenu']);
        // die;
        return view('Admin/settingApp', $data);
    }
    public function updateApp()
    {

        // dd($this->request->getVar());
        $namaapp = $this->request->getPost('namaapp');
        $telpapp = $this->request->getPost('telpapp');
        $logoapp = $this->request->getFile('logoapp');
        $deskripsi = $this->request->getPost('deskripsi');

        if (!$this->validate([
            'logoapp' => [
                'rules' => 'is_image[logoapp]|mime_in[logoapp,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'is_image' => 'yang anda pilih bukan gambar',
                    'mime_in' => 'yang anda pilih bukan gambar',
                ]
            ]
        ])) {
            $errors = $this->validation->getErrors();
            return redirect()->to('/settingapp')->withInput()->with('errors', $errors);
        }
        $fileGambar = $this->request->getFile('logoapp');
        //cek gambar apakah masih gambar lama?
        if ($fileGambar->getError() == 4) {
            $namaGambar = $this->request->getVar('logolama');
        } else {
            $namaGambar = $fileGambar->getRandomName();
            // upload gambar
            $fileGambar->move(ROOTPATH . 'public/assets/images/', $namaGambar);
            // hapus file lama

            $oldFilePath = ROOTPATH . 'public/assets/images/' . $this->request->getVar('logolama');
            if (file_exists($oldFilePath)) {
                unlink($oldFilePath);
            }
        }
        $data = [
            'namaapp' => $namaapp,
            'telpapp' => $telpapp,
            'logoapp' => $namaGambar,
            'deskripsi' => $deskripsi
        ];
        // var_dump($data);
        // die;
        $hasil = $this->settingApp->updateSetting('app', $data, 1);
        if ($hasil) {
            \session()->setFlashdata('success', 'Data Berhasil Di Update');
            // $hasil = $this->settingApp->getSetting('*', 'app', ["statusapp" => 1], '', '');
            // $data2 = [
            //     'hasil' => $hasil,
            // ];
            return redirect()->to('/settingapp');
            // return view('Admin/settingApp', $data2);
        } else {
            return false;
        }

        // Validate the uploaded file
        // if ($logoapp && $logoapp->isValid() && in_array($logoapp->getExtension(), ['jpg', 'jpeg']) || $logoapp = "") {
        //     // Move the uploaded file to the desired directory
        //     $oldFilePath = ROOTPATH . 'public/assets/images/1689703726_394ed4c950adcdb573ef.jpg'; // Replace with the actual file path

        //     // Remove the old file if it exists
        //     if (file_exists($oldFilePath)) {
        //         unlink($oldFilePath);
        //     }
        //     $newFilePath =  $logoapp->getRandomName();
        //     $logoapp->move(ROOTPATH . 'public/assets/images', $newFilePath);
        //     // dd("berhasil");
        //     $data = [
        //         'namaapp' => $namaapp,
        //         'telpapp' => $telpapp,
        //         'logoapp' => $logoapp,
        //         'deskripsi' => $deskripsi
        //     ];
        //     // var_dump($data);
        //     // die;
        //     $this->settingApp->updateSetting('app', $data, 1);
        //     \session()->setFlashdata('success', 'Data Berhasil Di Update');
        //     $hasil = $this->settingApp->getSetting('*', 'app', ["statusapp" => 1], '', '');
        //     $data2 = [
        //         'hasil' => $hasil,
        //     ];
        //     return view('Admin/settingApp', $data2);
        // } else {
        //     dd("error");
        // }
    }
}

<?php

namespace App\Controllers;

// use DateTime;
// use App\ThirdParty\PHPMailer\src\PHPMailer;
// use App\ThirdParty\PHPMailer\src\Exception;
// use App\ThirdParty\PHPMailer\src\SMTP;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use App\Models\Penomoran;
use CodeIgniter\Database\Query;

class Home extends BaseController
{
    protected  $validation;
    protected $activasi;
    protected $penomoran;
    protected $db;
    public function __construct()
    {
        $this->penomoran = new Penomoran();
        $this->validation = \Config\Services::validation();
        $this->db = \Config\Database::connect();
    }
    public function index()
    {
        // $hasil  = $this->menu->getSemuaMenu('*', 'menu', ["status" => "1"], 'kdmenu', '');

        $infoApp = $this->penomoran->getData('*', 'app', ["statusapp" => "1"], '', '');
        // dd($infoApp);
        $data = [
            'infoApp' => $infoApp,
        ];
        return view('tamplate/layout', $data);
    }
    public function login()
    {
        $data = [
            'judul' => 'Login',
        ];
        return view('halamanawal/login', $data);
    }
    public function daftar()
    {
        $data = [
            'judul' => 'Daftar Akun Baru',
        ];
        return view('halamanawal/daftarbaru', $data);
    }
    public function daftarpengguna()
    {

        $Rules = [
            'namalansia' => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukan Nama lengkap Lansia',
                ],
            ],
            'kodepanti' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Kode Unique Panti Anda',
                ],
            ],
            'email' => [
                'rules'  => 'required|valid_email|is_unique[user.email]',
                'errors' => [
                    'required' => 'Masukan email Anda Yang Aktif',
                    'valid_email' => 'Masukan email yang valid',
                    'is_unique' => 'Email Sudah terdaftar',
                ],
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Password Anda',
                ],
            ],
            'repeat_password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Ulang Password Anda',
                ],
            ],
        ];
        if (!$this->validate($Rules)) {
            $errors = $this->validation->getErrors();
            $data = [
                'judul' => 'Daftar Akun Baru',
                'errors' => $errors,
            ];
            return redirect()->back()->withInput()->with('errors', $errors)->with('data', $data);
        } else {
            $namalansia = strtoupper($this->request->getPost('namalansia'));
            $kodepanti = $this->request->getPost('kodepanti');
            $namaterapis = $this->request->getPost('kodepanti');
            $email = $this->request->getPost('email');
            $pass = $this->request->getPost('password');
            $create = date('Ymdhis');
            // dd($namalansia);
            $data = [
                // 'namapanti' => $namapanti,
                //   'namaterapis' => $namaterapis,
                'email' => $email,
                //  'image' => 'default.jpg',
                'kodeuniq' => $kodepanti,
                'password' => $pass,
                'roleid' => 2,
                'isactive' => 0,
                'datecreate' => $create
            ];
            $cekdata = $this->penomoran->getData('*', 'user', ["isactive" => "1", "kodeuniq" => "$kodepanti"], '', '');
            $cekdatalansia = $this->penomoran->getData('*', 'userprofil', ["status" => "1", "nama" => "$namalansia"], '', '');
            // dd($cekdatalansia[0]['nama']);
            // $data2 = [
            //     'iduser' => $cekdata[0]['id'],
            //     'nama' => $namalansia,
            //     'alamat' => '',
            //     'image' => 'default.jpg',
            //     'kodeuniq' => $kodepanti,
            //     'password' => $pass,
            //     'roleid' => 2,
            //     'isactive' => 0,
            //     'datecreate' => $create
            // ];
            // dd($cekdata);
            if (empty($cekdata)) {
                // Array $cekdata kosong, tampilkan pesan gagal atau lakukan tindakan lain sesuai kebutuhan Anda.
                \session()->setFlashdata('error', 'Kode Uniq anda tidak valid!');
                return redirect()->back()->withInput();
            } elseif (empty($cekdatalansia)) {
                \session()->setFlashdata('error', 'Nama Lansia anda tidak valid!');
                return redirect()->back()->withInput();
            } else {
                // Array $cekdata tidak kosong, lakukan operasi lanjutan di sini.

                $this->db->table('user')->insert($data);
                // if ($masuk) {
                //     $this->db->table('userprofil')->insert($data2);
                // }
            }

            // dd($cekdata);

            $data['namaketuapanti'] = $namalansia;
            //fungsi kirim email
            $this->_sendEmail($data);
            // echo $namaketuapanti, $namapanti, $namaterapis, $nomerhp, $email, $alamat, $pass;
            $data2 = [
                'judul' => 'Login',
            ];
            \session()->setFlashdata('success', 'Pendaftaran akun berhasil. Silahkan cek Email Anda untuk aktivasi akun!');
            // return view('halamanawal/login', $data2);
            return redirect()->to('login');
        }
    }
    public function proses()
    {

        $Rules = [
            'namapanti' => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Panti',
                ],
            ],
            'namaketuapanti' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Ketua Panti',
                ],
            ],
            'namaterapis' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Terapis',
                ],
            ],
            'nomerhp' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Nomer Hp Anda',
                ],
            ],
            'email' => [
                'rules'  => 'required|valid_email|is_unique[user.email]',
                'errors' => [
                    'valid_email' => 'Masukan email yang valid',
                    'is_unique' => 'Email Sudah terdaftar',
                ],
            ],
            'alamat' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Alamat Anda',
                ],
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Password Anda',
                ],
            ],
            'repeat_password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Ulang Password Anda',
                ],
            ],
        ];
        if (!$this->validate($Rules)) {
            $errors = $this->validation->getErrors();
            $data = [
                'judul' => 'Daftar Akun Baru',
                'errors' => $errors,
            ];
            return redirect()->back()->withInput()->with('errors', $errors)->with('data', $data);
        } else {
            $namapanti = $this->request->getPost('namapanti');
            $namaketuapanti = $this->request->getPost('namaketuapanti');
            $namaterapis = $this->request->getPost('namaterapis');
            $nomerhp = $this->request->getPost('nomerhp');
            $email = $this->request->getPost('email');
            $alamat = $this->request->getPost('alamat');
            $pass = $this->request->getPost('password');
            $create = date('Ymdhis');
            $kodeuniq = base64_encode(date('Ymdhis'));
            $data = [
                'namapanti' => $namapanti,
                'namaketuapanti' => $namaketuapanti,
                'namaterapis' => $namaterapis,
                'alamat' => $alamat,
                'nomerhp' => $nomerhp,
                'email' => $email,
                'image' => 'default.jpg',
                'kodeuniq' =>  $kodeuniq,
                'password' => $pass,
                'roleid' => 2,
                'isactive' => 0,
                'datecreate' => $create
            ];
            $this->db->table('user')->insert($data);
            //fungsi kirim email
            $this->_sendEmail($data);
            // echo $namaketuapanti, $namapanti, $namaterapis, $nomerhp, $email, $alamat, $pass;
            $data2 = [
                'judul' => 'Login',
            ];
            \session()->setFlashdata('success', 'Pendaftaran akun berhasil. Silahkan cek Email Anda untuk aktivasi akun!');
            // return view('halamanawal/login', $data2);
            return redirect()->to('login');
        }
    }
    private function _sendEmail($data)
    {

        $email = $data['email'];
        $nama = base64_encode($data['namaketuapanti']);
        $code = base64_encode($data['datecreate']);
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = SMTP::DEBUG_OFF;                      //Enable verbose debug output
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->Username   = 'activationlansia@gmail.com';                     //SMTP username
            $mail->Password   = 'cqbsoidwvriysxyt';                               //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            //Recipients
            $mail->setFrom('activavationlansia@lansia.com', 'Activation Acount');
            $mail->addAddress($email, $nama);     //Add a recipient

            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = 'Activation Acount';
            $mail->Body    = 'Klik link  untuk activasi Acount  
            <a href="' . base_url("/") . 'updateuser?email=' . $email . '&token=' . $code . '&username=' . $nama . '" ><b>Verifikasi Akun</b></href>';
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            if ($mail->send()) {
                return true;
            }
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
    // private function _sendEmail1($data)
    // {
    //     dd($data);
    //     $email = $data['email'];
    //     $nama = base64_encode($data['nama']);
    //     $code = base64_encode($data['datecreate']);
    //     $mail = new PHPMailer(true);

    //     try {
    //         //Server settings
    //         $mail->SMTPDebug = SMTP::DEBUG_OFF;                      //Enable verbose debug output
    //         $mail->isSMTP();                                            //Send using SMTP
    //         $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
    //         $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    //         $mail->Username   = 'activationlansia@gmail.com';                     //SMTP username
    //         $mail->Password   = 'cqbsoidwvriysxyt';                               //SMTP password
    //         $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    //         $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //         //Recipients
    //         $mail->setFrom('activavationlansia@lansia.com', 'Activation Acount');
    //         $mail->addAddress($email, $nama);     //Add a recipient

    //         //Content
    //         $mail->isHTML(true);                                  //Set email format to HTML
    //         $mail->Subject = 'Activation Acount';
    //         $mail->Body    = 'Klik link  untuk activasi Acount  
    //         <a href="' . base_url("/") . 'updateuser?email=' . $email . '&token=' . $code . '&username=' . $nama . '" ><b>Verifikasi Akun</b></href>';
    //         $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    //         if ($mail->send()) {
    //             return true;
    //         }
    //     } catch (Exception $e) {
    //         echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    //     }
    // }
    public function updateuser()
    {
        $token = base64_decode($_GET['token']);
        $email = $_GET['email'];
        $username = base64_decode($_GET['username']);
        $table = 'user';
        $field = 'id,namaketuapanti,email,datecreate';
        $cari =  ["email" => $email, "namaketuapanti" => $username, 'datecreate' => $token];
        $query = $this->db->table($table)
            ->select($field)
            ->where($cari)
            // ->orderBy($orderBy, $short)
            // ->get()
            ->countAllResults();

        if ($query > 0) {
            $this->db->table('user')
                ->set('isactive', 1)
                ->where('email', $email)
                ->update();
            return redirect()->to('login');
        } else {
            echo 'Maaf email atau Token Anda tidak Valid';
        }
    }
    public function ceklogin()
    {

        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        $table = 'user';
        $field = '*';
        $cari =  ["email" => $email,  'password' => $password, 'isactive' => 1];
        $query = $this->db->table($table)
            ->select($field)
            ->where($cari)
            // ->orderBy($orderBy, $short)
            ->get();
        // ->countAllResults();
        // if ($query > 0) {

        //     \session()->setFlashdata('succes', 'Login Berhasil');

        //     return redirect()->to('/user');
        // } else {
        //     // $errors = $this->validation->getErrors();
        //     \session()->setFlashdata('errors', 'Maaf Username atau Password salah!');
        //     return redirect()->back()->withInput();;
        // }
        if ($query->getNumRows() > 0) {
            $user = $query->getRow(); // Mendapatkan satu baris data pengguna

            // Set session data
            session()->set('logged_in', true);
            session()->set('user', $user); // Menyimpan data pengguna dalam session
            session()->setFlashdata('success', 'Login Berhasil');

            return redirect()->to('/user');
        } else {
            // Set session data
            session()->set('logged_in', false);
            session()->setFlashdata('errors', 'Maaf Username atau Password salah!');
            return redirect()->back()->withInput();
        }

        // dd($this->request->getPost());
        // echo "sini";
    }
    public function logout()
    {
        // Destroy the session
        session()->destroy();

        // Redirect to the login page
        return redirect()->to('/login');
    }
}

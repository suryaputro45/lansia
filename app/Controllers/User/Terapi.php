<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\Penomoran;
use App\Models\SuperAdmin\Menu;


class Terapi extends BaseController
{
    protected $penomoran;
    protected $menu;
    protected $validation;
    protected $db;
    protected $ambildata;
    protected $terapi;
    protected $status;
    protected $getJoinData;

    public function __construct()
    {
        $userData = session()->get('user');
        $this->penomoran = new Penomoran();
        $this->menu = new Menu();
        $this->validation = \Config\Services::validation();
        $this->db = \Config\Database::connect();
        $this->ambildata = $this->penomoran->getData('*', 'userprofil', ["status" => 1, "idpanti" => "$userData->id"], '', '');
        $this->terapi = $this->penomoran->getData('idterapi,namaterapi', 'terapi', ["status" => 1], '', '');
        $this->status = $this->penomoran->getData('idstatus,namastatus', 'statusterapis', ["status" => 1], '', '');
        $this->getJoinData = $this->penomoran->getDataJoin2(
            '*,userprofil.idprofil as kode',
            'userprofil',
            'profiltran',
            'userprofil.idprofil=profiltran.idprofil',
            ['userprofil.status' => 1, "userprofil.idpanti" => "$userData->id"],
            '',
            ''
        );
    }
    public function index()
    {
        // dd($this->getJoinData);
        $hasil  = $this->menu->getSemuaMenu('*', 'menu', ["status" => "1"], 'kdmenu', '');

        // dd($ambildata);
        $data = [
            'data' => $this->getJoinData,
            'pengguna' => $this->ambildata
        ];
        // getData($field = null, $table = null, $cari = [],  $orderBy = null, $short = null)
        // var_dump($hasil[0]['kdmenu']);
        // die;
        return view('user/terapi', $data);
    }
    public function dataterapi()
    {
        // echo $num;
        // $pasien = $this->penomoran->getData('idprofil,nama,alamat,notelp', 'userprofil', ["status" => 1, "idprofil" => $num], '', '');
        // $tranterapi = $this->penomoran->getDatajoin2('*,terapi.idterapi as kodeterapi,terapitran.status as statustran', 'terapi', 'terapitran', 'terapitran.idterapi=terapi.idterapi', ["terapi.status" => 1, "idprofil" => $num], '', '');
        $tranter = $this->penomoran->getData('*', 'terapi', ["status" => 1], '', '');
        // dd($tranter);

        $data = [
            // 'data' => $pasien,
            'terapi' => $tranter,
            // 'trasaksi' => $tranterapi
        ];
        // getData($field = null, $table = null, $cari = [],  $orderBy = null, $short = null)
        // var_dump($data);
        // die;
        //  return view('user/detailData', $data);
        // getData($field = null, $table = null, $cari = [],  $orderBy = null, $short = null)
        // var_dump($hasil[0]['kdmenu']);
        // die;
        return view('user/kategoriterapi', $data);
    }
    public function updateTerapi($id)
    {
        //ngambil data dari table terapi(jenis)
        // $pasien = $this->penomoran->getData('idprofil,nama,alamat,notelp', 'userprofil', ["status" => 1, "idprofil" => $num], '', '');
        $dataterapi = $this->penomoran->getData('*', 'terapi', ['status' => 1, 'idterapi' => $id], '', '');
        // dd($dataterapi);
        $data = [
            'data' => $dataterapi
        ];


        // dd($id);
        return view('user/terapiedit', $data);
    }
}

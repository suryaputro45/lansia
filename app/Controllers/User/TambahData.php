<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\Penomoran;
use App\Models\SuperAdmin\Menu;
use CodeIgniter\I18n\Time;
use SebastianBergmann\CodeUnit\FunctionUnit;

class TambahData extends BaseController
{
    protected $penomoran;
    protected $menu;
    protected $validation;
    protected $db;
    protected $ambildata;
    protected $terapi;
    protected $status;
    protected $getJoinData;
    public function __construct()
    {
        $userData = session()->get('user');
        $this->penomoran = new Penomoran();
        $this->menu = new Menu();
        $this->validation = \Config\Services::validation();
        $this->db = \Config\Database::connect();
        $this->ambildata = $this->penomoran->getData('*', 'userprofil', ["status" => 1, "idpanti" => "$userData->id"], '', '');
        $this->terapi = $this->penomoran->getData('idterapi,namaterapi', 'terapi', ["status" => 1], '', '');
        $this->status = $this->penomoran->getData('idstatus,namastatus', 'statusterapis', ["status" => 1], '', '');
        $this->getJoinData = $this->penomoran->getDataJoin2(
            '*,userprofil.idprofil as kode',
            'userprofil',
            'profiltran',
            'userprofil.idprofil=profiltran.idprofil',
            ['userprofil.status' => 1, "userprofil.idpanti" => "$userData->id"],
            '',
            ''
        );
    }

    public function index()
    {
        // dd($this->getJoinData);
        $hasil  = $this->menu->getSemuaMenu('*', 'menu', ["status" => "1"], 'kdmenu', '');

        // dd($ambildata);
        $data = [
            'data' => $this->getJoinData
        ];
        // getData($field = null, $table = null, $cari = [],  $orderBy = null, $short = null)
        // var_dump($hasil[0]['kdmenu']);
        // die;
        return view('user/tambahData', $data);
    }
    public function newData()
    {
        // echo "kesini";
        $hasil  = $this->menu->getSemuaMenu('*', 'menu', ["status" => "1"], 'kdmenu', '');
        // var_dump($hasil[0]['kdmenu']);
        $data = [
            'data' =>  $this->ambildata
        ];
        // die;
        return view('user/newData', $data);
    }

    public function saveBaru()
    {
        $userData = session()->get('user');
        $tanggal = Time::now();
        $datecreate = $tanggal->getTimestamp();
        // $dateupdate = $tanggal->getTimestamp();
        // dd($tanggal_integer);
        $Rules = [
            'namapasien' => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Pasien',
                ],
            ],
            'alamatpasien' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Alamat Pasien',
                ],
            ],
            'telppasien' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Nomer Telpon',
                ],
            ],

        ];
        if (!$this->validate($Rules)) {
            // echo "masak?";
            $errors = $this->validation->getErrors();
            $data = [
                'judul' => 'Daftar Akun Baru',
                'errors' => $errors,
            ];
            return redirect()->back()->withInput()->with('errors', $errors)->with('data', $data);
        } else {
            $nama = $this->request->getVar('namapasien');
            $alamat = $this->request->getVar('alamatpasien');
            $telp = $this->request->getVar('telppasien');
            $tempatlhr = $this->request->getVar('tempatlahir');
            $tanggallhr = date("Y-m-d", strtotime($this->request->getVar('tanggallahir')));
            $pasangan = $this->request->getVar('namapasangan');
            $namaanak = $this->request->getVar('namaanak');
            $namasaudara = $this->request->getVar('namasaudara');
            $kesantempat = $this->request->getVar('namaingattempat');
            // $telppasien = $this->request->getVar('telppasien');
            //gambar
            $fotolansia = $this->request->getFile('fotolansia');
            $fotoanak = $this->request->getFile('fotoanak');
            $fotorumah = $this->request->getFile('fotorumah');
            $fotopasangan = $this->request->getFile('fotopasangan');
            $fotosaudara = $this->request->getFile('fotosaudara');
            $fotoingattempat = $this->request->getFile('fotoingatempat');
            // dd($this->request->getFile('fotolansia'));
            if ($fotolansia->getError() == 4) {
                // dd("sini");
                $namafotolansia = $this->request->getVar('logolama');
            } else {
                // dd('sana');
                $namafotolansia = $fotolansia->getRandomName();
                // upload gambar
                $fotolansia->move(ROOTPATH . 'public/assets/images/', $namafotolansia);
                // hapus file lama

                // $oldFilePath = ROOTPATH . 'public/assets/images/' . $this->request->getVar('logolama');
                // if (file_exists($oldFilePath)) {
                //     unlink($oldFilePath);
                // }
            }
            if ($fotorumah->getError() == 4) {
                $namafotorumah = $this->request->getVar('logolama');
            } else {
                $namafotorumah = $fotorumah->getRandomName();
                // upload gambar
                $fotorumah->move(ROOTPATH . 'public/assets/images/', $namafotorumah);
                // hapus file lama

                // $oldFilePath = ROOTPATH . 'public/assets/images/' . $this->request->getVar('logolama');
                // if (file_exists($oldFilePath)) {
                //     unlink($oldFilePath);
                // }
            }
            if ($fotopasangan->getError() == 4) {
                $namafotopasangan = $this->request->getVar('logolama');
            } else {
                $namafotopasangan = $fotopasangan->getRandomName();
                // upload gambar
                $fotopasangan->move(ROOTPATH . 'public/assets/images/', $namafotopasangan);
                // hapus file lama

                // $oldFilePath = ROOTPATH . 'public/assets/images/' . $this->request->getVar('logolama');
                // if (file_exists($oldFilePath)) {
                //     unlink($oldFilePath);
                // }
            }
            if ($fotosaudara->getError() == 4) {
                $namafotosaudara = $this->request->getVar('logolama');
            } else {
                $namafotosaudara = $fotosaudara->getRandomName();
                // upload gambar
                $fotosaudara->move(ROOTPATH . 'public/assets/images/', $namafotosaudara);
                // hapus file lama

                // $oldFilePath = ROOTPATH . 'public/assets/images/' . $this->request->getVar('logolama');
                // if (file_exists($oldFilePath)) {
                //     unlink($oldFilePath);
                // }
            }
            if ($fotoanak->getError() == 4) {
                $namafotoanak = $this->request->getVar('logolama');
            } else {
                $namafotoanak = $fotoanak->getRandomName();
                // upload gambar
                $fotoanak->move(ROOTPATH . 'public/assets/images/', $namafotoanak);
                // hapus file lama

                // $oldFilePath = ROOTPATH . 'public/assets/images/' . $this->request->getVar('logolama');
                // if (file_exists($oldFilePath)) {
                //     unlink($oldFilePath);
                // }
            }
            if ($fotoingattempat->getError() == 4) {
                $namafotoingattempat = $this->request->getVar('logolama');
            } else {
                $namafotoingattempat = $fotoingattempat->getRandomName();
                // upload gambar
                $fotoingattempat->move(ROOTPATH . 'public/assets/images/', $namafotoingattempat);
                // hapus file lama

                // $oldFilePath = ROOTPATH . 'public/assets/images/' . $this->request->getVar('logolama');
                // if (file_exists($oldFilePath)) {
                //     unlink($oldFilePath);
                // }
            }
            //user profil
            $data = [
                'idpanti' => $userData->id,
                'nama' => $nama,
                'namapasangan' => $pasangan,
                'fotopasangan' => $namafotopasangan,
                'tempatlahir' => $tempatlhr,
                'tanggallahir' => $tanggallhr,
                'alamat' => $alamat,
                'notelp' => $telp,
                'foto' => $namafotolansia,
                'fotorumah' => $namafotorumah,
                'datecreate' => $datecreate,
                'dateupdate' => "",
                'status' => 1,
            ];

            $simpan = $this->db->table('userprofil')->insert($data);
            // $idprofil = insertGetId($simpan);
            // dd($data);
            // profiltran

            if ($simpan) {
                // echo "apa";
                $idprofil = $this->db->insertID();
                $data1 = [
                    "idprofil" => $idprofil,
                    "anak" => $namaanak,
                    "saudara" => $namasaudara,
                    "kesantempat" => $kesantempat,
                    "fotoanak" => $namafotoanak,
                    "fotosaudara" => $namafotosaudara,
                    "fotokesantempat" => $namafotoingattempat,
                    "status" => 1,
                ];
                $simpantran = $this->db->table('profiltran')->insert($data1);
                if ($simpantran) {
                    $data = [
                        'data' => $this->ambildata
                    ];
                    \session()->setFlashdata('success', 'Data Berhasil di Tambahkan!');
                    return redirect()->to('/tambahdata');
                    // return view('user/tambahData', $data);
                }
            } else {
                \session()->setFlashdata('error', 'gagal');

                return view('user/tambahData');
            }
        }

        // echo $nama;
    }
    public function updateData()
    {

        $Rules = [
            'namapasien' => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Pasien',
                ],
            ],
            'alamatpasien' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Alamat Pasien',
                ],
            ],
            'telppasien' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Nomer Telpon',
                ],
            ],

        ];
        if (!$this->validate($Rules)) {
            // echo "masak?";
            $errors = $this->validation->getErrors();
            $data = [
                'judul' => 'Daftar Akun Baru',
                'errors' => $errors,
            ];
            return redirect()->back()->withInput()->with('errors', $errors)->with('data', $data);
        } else {
            $id = $this->request->getVar('idpasien');
            $nama = $this->request->getVar('namapasien');
            $alamat = $this->request->getVar('alamatpasien');
            $telp = $this->request->getVar('telppasien');
            $data = [
                'nama' => $nama,
                'alamat' => $alamat,
                'nomorhp' => $telp,
                'status' => 1
            ];

            $update = $this->db->table('pasien')->set($data)->where('idpasien', $id)->update();
            // dd($data);
            $data2 = [
                'data' => $this->ambildata
            ];
            if ($update) {
                // echo "apa";
                \session()->setFlashdata('success', 'Pendaftaran akun berhasil. Silahkan cek Email Anda untuk aktivasi akun!');
                return  redirect()->to('/tambahdata');
            } else {
                return view('user/tambahData');
            }
        }

        // echo $nama;
    }
    public function hapusData()
    {
        $id = $this->request->getVar('idpasien');
        $hapus = $this->db->table('pasien')->where('idpasien', $id)->delete();
        if ($hapus) {
            // echo "apa";
            \session()->setFlashdata('success', 'Pendaftaran akun berhasil. Silahkan cek Email Anda untuk aktivasi akun!');
            return  redirect()->to('/tambahdata');
        } else {
            return view('user/tambahData');
        }
    }
    public function newTerapi()
    {
        // echo "kesini";
        $hasil  = $this->menu->getSemuaMenu('*', 'menu', ["status" => "1"], 'kdmenu', '');
        // var_dump($hasil[0]['kdmenu']);
        // die;
        $data = [
            'data' =>  $this->ambildata,
            'terapi' => $this->terapi,
            'status' => $this->status
        ];
        return view('user/newTerapi', $data);
    }
    public function saveterapi()
    {

        $Rules = [
            'namapasien' => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Pasien',
                ],
            ],
            'namaterapi' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Alamat Pasien',
                ],
            ],
            'statusterapi' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan statusterapi',
                ],
            ],
            'document1' => [
                'rules' => 'uploaded[document1]|ext_in[document1,pdf,jpg,jpeg,png]',
                'errors' => [
                    'uploaded' => 'Pilih file untuk diunggah.',
                    'ext_in' => 'Hanya file PDF, JPG, JPEG, atau PNG yang diizinkan.',
                ]
            ],
        ];


        if (!$this->validate($Rules)) {

            dd($this->request->getVar('document1'), $this->validation->getErrors());
            // echo "masak?";
            $errors = $this->validation->getErrors();
            $data = [
                'judul' => 'Daftar Akun Baru',
                'errors' => $errors,
            ];
            return redirect()->back()->withInput()->with('errors', $errors)->with('data', $data);
        } else {
            $fileGambar = $this->request->getFile('document1');
            $fileGambar2 = $this->request->getFile('document2');

            $document1 = $fileGambar->getRandomName();
            // upload gambar
            $fileGambar->move(ROOTPATH . 'public/assets/document/', $document1);
            $document2 = $fileGambar2->getRandomName();
            // upload gambar
            // $fileGambar->move(ROOTPATH . 'public/assets/document/', $document2);




            // dd($fileGambar);

            $nama = $this->request->getVar('namapasien');
            $alamat = $this->request->getVar('namaterapi');
            $telp = $this->request->getVar('statusterapi');
            $data = [
                'idapp' => 1,
                'idpasien' => $nama,
                'idterapi' => $alamat,
                'statusterapis' => $telp,
                'document1' => $document1,
                'document2' => $document2,
                'document3' => '',
                'status' => 1
            ];

            $simpan = $this->db->table('transaksiterapis')->insert($data);
            // dd($data);
            if ($simpan) {
                // echo "apa";
                \session()->setFlashdata('success', 'Data Berhasil Ditambahkan');
                // return view('user/tambahData');
                return redirect()->to('/user');
            } else {
                return view('user/tambahData');
            }
        }

        // echo $nama;
    }
    public function updateProfil()
    {

        $Rules = [
            'namapasien' => [
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Pasien',
                ],
            ],
            'alamatpasien' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Alamat Pasien',
                ],
            ],
            'telppasien' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Masukan Nama Nomer Telpon',
                ],
            ],

        ];
        if (!$this->validate($Rules)) {
            // echo "masak?";
            $errors = $this->validation->getErrors();
            $data = [
                'judul' => 'Daftar Akun Baru',
                'errors' => $errors,
            ];
            return redirect()->back()->withInput()->with('errors', $errors)->with('data', $data);
        } else {
            $id = $this->request->getVar('idpasien');
            $nama = $this->request->getVar('namapasien');
            $alamat = $this->request->getVar('alamatpasien');
            $telp = $this->request->getVar('telppasien');
            $data = [
                'nama' => $nama,
                'alamat' => $alamat,
                'nomorhp' => $telp,
                'status' => 1
            ];

            $update = $this->db->table('user')->set($data)->where('idpasien', $id)->update();
            // dd($data);
            // $data2 = [
            //     'data' => $this->ambildata
            // ];
            if ($update) {
                // echo "apa";
                \session()->setFlashdata('success', 'Pendaftaran akun berhasil. Silahkan cek Email Anda untuk aktivasi akun!');
                return  redirect()->to('/profil');
            } else {
                return view('user/profil');
            }
        }

        // echo $nama;
    }
    public function DetailPasien($num)
    {

        // echo $num;
        $pasien = $this->penomoran->getData('idprofil,nama,alamat,notelp', 'userprofil', ["status" => 1, "idprofil" => $num], '', '');
        $tranterapi = $this->penomoran->getDatajoin2('*,terapi.idterapi as kodeterapi,terapitran.status as statustran', 'terapi', 'terapitran', 'terapitran.idterapi=terapi.idterapi', ["terapi.status" => 1, "idprofil" => $num], '', '');
        $tranter = $this->penomoran->getData('*', 'terapi', ["status" => 1], '', '');
        // dd($tranter);

        $data = [
            'data' => $pasien,
            'terapi' => $tranter,
            'trasaksi' => $tranterapi
        ];
        // getData($field = null, $table = null, $cari = [],  $orderBy = null, $short = null)
        // var_dump($data);
        // die;
        return view('user/detailData', $data);
    }
    public function tampilData($id)
    {
        $pasien = $this->penomoran->getData('*', 'userprofil', ["status" => 1, "idprofil" => $id], '', '');
        $data = [
            'data' => $pasien
        ];
        // dd($pasien);
        // echo "okay";
        return view('user/tampilData', $data);
    }
}

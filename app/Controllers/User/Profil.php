<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\Penomoran;
use App\Models\SuperAdmin\Menu;


class Profil extends BaseController
{
    protected $penomoran;
    protected $menu;
    public function __construct()
    {
        $this->penomoran = new Penomoran();
        $this->menu = new Menu();
    }

    public function index()
    {
        // Retrieve user data from session or database
        $userData = session()->get('user');
        // dd($userData);
        // $hasil  = $this->menu->getSemuaMenu('*', 'menu', ["status" => "1"], 'kdmenu', '');
        // var_dump($hasil[0]['kdmenu']);
        // die;
        $data = [
            "user" => $userData
        ];

        return view('user/profil', $data);
    }
}

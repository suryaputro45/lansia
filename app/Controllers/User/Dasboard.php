<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\Penomoran;
use App\Models\SuperAdmin\Menu;


class Dasboard extends BaseController
{
    protected $penomoran;
    protected $menu;
    protected $validation;
    protected $db;
    protected $pasien;
    protected $transaksi;

    public function __construct()
    {
        $this->penomoran = new Penomoran();
        $this->menu = new Menu();
        $this->validation = \Config\Services::validation();
        $this->db = \Config\Database::connect();
        $this->pasien = $this->penomoran->getData('idpasien,nama,alamat,nomorhp', 'pasien', ["status" => 1], '', '');
        $this->transaksi = $this->penomoran->getDataJoin(
            'idtranterapis,nama,alamat,nomorhp,statusterapis.namastatus as status,terapi.namaterapi as namaterapi',
            'transaksiterapis',
            'pasien',
            'statusterapis',
            'terapi',
            'transaksiterapis.idpasien=pasien.idpasien',
            'transaksiterapis.statusterapis=statusterapis.idstatus',
            'transaksiterapis.idterapi =terapi.idterapi',
            ["transaksiterapis.status" => 1],
            '',
            ''
        );
    }

    public function index()
    {

        $userData = session()->get('user');
        $hasil  = $this->menu->getSemuaMenu('*', 'menu', ["status" => "1"], 'kdmenu', '');
        $cekdatalansia = $this->penomoran->getData('*', 'userprofil', ["status" => "1", "idpanti" => "$userData->id"], '', '');
        // dd(count($cekdatalansia));
        // var_dump($this->transaksi);
        // die;
        $data = [
            'data' => $userData,
            'lansia' => count($cekdatalansia),
        ];
        return view('user/dasboard', $data);
    }
}

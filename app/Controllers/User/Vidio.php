<?php

namespace App\Controllers\User;

use App\Controllers\BaseController;
use App\Models\Penomoran;
use App\Models\Crud;
use App\Models\SuperAdmin\Menu;


class Vidio extends BaseController
{
    protected $penomoran;
    protected $menu;
    protected $validation;
    protected $db;
    protected $ambildata;
    protected $terapi;
    protected $status;
    protected $userData;
    protected $masuk;
    // protected $db;
    public function __construct()
    {
        $this->userData = session()->get('user');
        $this->penomoran = new Penomoran();
        $this->masuk = new Crud();
        $this->validation = \Config\Services::validation();
        $this->db = \Config\Database::connect();
    }
    public function index($id, $idprofil)
    {
        switch (TRUE) {
            case ($id == 1):
                $data = [
                    'video' => 'https://www.youtube.com/embed/0I8qeXFrOeI?si=EcGsoCjCdjaQRoRT',
                    'judul' => 'hari pertama',
                    'id' => $id,
                    'idprofil' => $idprofil
                ];
                break;
            case ($id == 2):
                $data = [
                    'video' => 'https://www.youtube.com/embed/0I8qeXFrOeI?si=EcGsoCjCdjaQRoRT',
                    'judul' => 'hari Kedua',
                    'id' => $id,
                    'idprofil' => $idprofil

                ];
                break;
            case ($id == 3):
                $data = [
                    'video' => 'https://www.youtube.com/embed/0I8qeXFrOeI?si=EcGsoCjCdjaQRoRT',
                    'judul' => 'hari ketiga',
                    'id' => $id,
                    'idprofil' => $idprofil

                ];
                break;
            case ($id == 4):
                $data = [
                    'video' => 'https://www.youtube.com/embed/0I8qeXFrOeI?si=EcGsoCjCdjaQRoRT',
                    'judul' => 'hari keempat',
                    'id' => $id,
                    'idprofil' => $idprofil

                ];
                break;
            case ($id == 5):
                $data = [
                    'video' => 'https://www.youtube.com/embed/0I8qeXFrOeI?si=EcGsoCjCdjaQRoRT',
                    'judul' => 'hari kelima',
                    'id' => $id,
                    'idprofil' => $idprofil

                ];
                break;
            case ($id == 6):
                $data = [
                    'video' => 'https://www.youtube.com/embed/0I8qeXFrOeI?si=EcGsoCjCdjaQRoRT',
                    'judul' => 'hari keenam',
                    'id' => $id,
                    'idprofil' => $idprofil

                ];
                break;
        }
        return view('user/vidio', $data);
    }
    public function simpanVidio()
    {

        $kode = $this->request->getVar('kode');
        $profil = $this->request->getVar('profil');
        $sebelum = $kode - 1;
        // dd($sebelum);
        $user = $this->userData->id;
        $pasien = $this->penomoran->getData('idprofil,nama,alamat,notelp', 'userprofil', ["status" => 1, "idprofil" => $profil], '', '');
        $tranterapi = $this->penomoran->getDatajoin2('*,terapi.idterapi as kodeterapi,terapitran.status as statustran', 'terapi', 'terapitran', 'terapitran.idterapi=terapi.idterapi', ["terapi.status" => 1, "idprofil" => $profil], '', '');
        $cekid = $this->penomoran->getDatajoin2('*,terapi.idterapi as kodeterapi,terapitran.status as statustran', 'terapi', 'terapitran', 'terapitran.idterapi=terapi.idterapi', ["terapi.status" => 1, "idprofil" => $profil, 'terapitran.idterapi' => $kode], '', '');
        $max = $this->penomoran->getDatajoin2('max(terapitran.idterapi) as maxid', 'terapi', 'terapitran', 'terapitran.idterapi=terapi.idterapi', ["terapi.status" => 1, "idprofil" => $profil], '', '');
        // dd($tranterapi);
        $data = [
            'idprofil' => $profil,
            'idterapi' => $kode,
            'status' => 1,
            'catatan' => '',
            'catatan2' => ''
        ];
        if ($tranterapi == null) {
            $this->masuk->tambahData('terapitran', $data);
            \session()->setFlashdata('success', 'Data Berhasil di Tambahkan!');
            return redirect()->to('/detaildata' . '/' . $profil);
            // echo "maaf data sudah ada";
        } elseif ($tranterapi != null) {
            // dd($max);

            if ($cekid == null && $max[0]['maxid'] == $sebelum) {
                $this->masuk->tambahData('terapitran', $data);
                \session()->setFlashdata('success', 'Data Berhasil di Tambahkan!');
                return redirect()->to('/detaildata' . '/' . $profil);
            }
            // maka cek apakah kode terapi itu sudah ada?
            \session()->setFlashdata('errors', 'Data sebelumya Belum selesai');
            return redirect()->to('/detaildata' . '/' . $profil);
        }

        // dd($tranterapi);
        echo "ok";
    }
}

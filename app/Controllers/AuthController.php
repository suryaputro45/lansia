<?php


namespace App\Controllers;

use CodeIgniter\Controller;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use League\OAuth2\Client\Provider\Google;

class AuthController extends Controller
{
    public function authorize()
    {
        $clientId = '708557764096-g030g7ivdt05emq6bmossgpaq3kehmpt.apps.googleusercontent.com';
        $clientSecret = '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';
        $redirectUri = 'http://localhost:8080/callback';

        $provider = new Google([
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'redirectUri' => $redirectUri,
        ]);

        $authorizationUrl = $provider->getAuthorizationUrl();
        // var_dump($authorizationUrl);
        // die;
        $_SESSION['oauth2state'] = $provider->getState();

        return redirect()->to($authorizationUrl);
    }

    public function callback()
    {
        $clientId = '708557764096-g030g7ivdt05emq6bmossgpaq3kehmpt.apps.googleusercontent.com';
        $clientSecret = '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';
        $redirectUri = 'http://localhost:8080/callback';
        // var_dump($_GET);
        // die;
        if (empty($_GET['state']) || $_GET['state'] !== $_SESSION['oauth2state']) {
            die('Invalid state');
        }

        $provider = new Google([
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'redirectUri' => $redirectUri,
        ]);
        // $authorizationUrl = $provider->getAuthorizationUrl();
        // var_dump($authorizationUrl);
        // die;
        $accessToken = $provider->getAccessToken('authorization_code', [
            'code' => $_GET['code'],
        ]);

        // Gunakan accessToken untuk mengirim email
        try {
            $mail = new PHPMailer(true);
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->AuthType = 'XOAUTH2';
            $mail->oauthUserEmail = 'activationlansia@gmail.com';
            $mail->oauthClientId = $clientId;
            $mail->oauthClientSecret = $clientSecret;
            $mail->oauthAccessToken = $accessToken->getToken();
            $mail->oauthRefreshToken = $accessToken->getRefreshToken();
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;

            $mail->setFrom('activationlansia@gmail.com', 'Your Name');
            $mail->addAddress('Suryaputro45@gmail.com', 'Recipient Name');
            $mail->Subject = 'Email Subject';
            $mail->Body = 'Email body content';

            $mail->send();
            echo 'Email sent successfully.';
        } catch (Exception $e) {
            echo 'Error sending email: ' . $mail->ErrorInfo;
        }

        return redirect()->to('/');
    }
}

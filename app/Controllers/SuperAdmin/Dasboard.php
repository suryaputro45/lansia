<?php

namespace App\Controllers\SuperAdmin;

use App\Controllers\BaseController;
use App\Models\Penomoran;
use App\Models\SuperAdmin\Menu;


class Dasboard extends BaseController
{
    protected $penomoran;
    protected $menu;
    public function __construct()
    {
        $this->penomoran = new Penomoran();
        $this->menu = new Menu();
    }

    public function index()
    {
        $hasil  = $this->menu->getSemuaMenu('*', 'menu', ["status" => "1"], 'kdmenu', '');
        // var_dump($hasil[0]['kdmenu']);
        // die;
        return view('SuperAdmin/dasboard');
    }
}
